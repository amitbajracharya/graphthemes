<?php

function dusky_blog_customizer_upgrade_to_pro( $wp_customize ) {

	$wp_customize->add_section( new Dusky_Blog_Button_Control( $wp_customize, 'upgrade-to-pro',	array(
		'title'    => esc_html__( '★ Dusky Blog Pro ', 'dusky-blog' ),
		'type'	=> 'button',
		'pro_text' => esc_html__( 'Upgrade to Pro', 'dusky-blog' ),
		'pro_url'  => esc_url( 'https://graphthemes.com/dusky-blog/' ),
		'priority' => 1
	) )	);

	
}
add_action( 'customize_register', 'dusky_blog_customizer_upgrade_to_pro' );


function dusky_blog_enqueue_custom_admin_style() {
        wp_register_style( 'dusky-blog-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.css', false );
        wp_enqueue_style( 'dusky-blog-button' );

        wp_enqueue_script( 'dusky-blog-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.js', array( 'jquery' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'dusky_blog_enqueue_custom_admin_style' );