<?php

/* Default Post Snippet Author*/
function dusky_blog_get_default_post_snippet_author() {
    return true;
}

/* Default Post Snippet Category*/
function dusky_blog_get_default_post_snippet_category() {
    return true;
}

/* Default Post Snippet Comment*/
function dusky_blog_get_default_post_snippet_comment() {
    return true;
}

/* Default Post Snippet Date*/
function dusky_blog_get_default_post_snippet_date() {
    return true;
}

/* Default Post Snippet Featured Image*/
function dusky_blog_get_default_post_snippet_featured_image() {
    return true;
}

/* Default Post Snippet Tag*/
function dusky_blog_get_default_post_snippet_featured_image_size() {
    return esc_html__( 'large', 'dusky-blog' );
}

/* Default Post Snippet Tag*/
function dusky_blog_get_default_post_snippet_tag() {
    return true;
}

/* Default Post Snippet Excerpt Szie  */
function dusky_blog_get_default_post_snippet_excerpt_size() {
    return 25;
}

/* Default Post Snippet Read More */
function dusky_blog_get_default_post_snippet_show_hide_read_more() {
    return true;
}

/* Default Post Snippet Read More Text */
function dusky_blog_get_default_post_snippet_read_more_text() {
    return esc_html__( "Read More", 'dusky-blog' );
}

/* Default Post Snippet Social Share */
function dusky_blog_get_default_post_snippet_social_share() {
    return true;
}

/* Default Post Snippet Social Share Options*/
function dusky_blog_get_default_post_snippet_social_share_options() {
    return array();
}