<?php

/* Default Font Family */
function dusky_blog_get_default_site_identity_font_family() {    
    return esc_html__( "Lora", 'dusky-blog' );
}

function dusky_blog_get_default_main_font_family() {    
    return esc_html__( "Poppins", 'dusky-blog' );
}

function dusky_blog_get_default_secondary_font_family() {    
    return esc_html__( "Lora", 'dusky-blog' );
}