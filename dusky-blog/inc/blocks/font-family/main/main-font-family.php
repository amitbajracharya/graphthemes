<?php

add_action( 'customize_register', 'dusky_blog_main_font_family' );
function dusky_blog_main_font_family( $wp_customize ) {

    $wp_customize->add_setting( 'main_font_family', array(
        'default'     => dusky_blog_get_default_main_font_family(),
        'sanitize_callback' => 'dusky_blog_sanitize_google_fonts'
    ) );

    $wp_customize->add_control( 'main_font_family', array(
        'settings'    => 'main_font_family',
        'label'       =>  esc_html__( 'Primary Font', 'dusky-blog' ),
        'section'     => 'dusky_blog_font_customization_section',
        'type'        => 'select',
        'choices'     => dusky_blog_google_fonts( dusky_blog_free_pro() ),
    ) );

}



add_action( 'wp_enqueue_scripts', 'dusky_blog_main_font_family_dynamic_css' );
function dusky_blog_main_font_family_dynamic_css() {

    $main_font_family = esc_attr( get_theme_mod( 'main_font_family', dusky_blog_get_default_main_font_family() ) );

    $dynamic_css = ":root { --primary-font: $main_font_family; }";

    wp_add_inline_style( 'dusky-blog', $dynamic_css );
}