<?php

/* Default General Customization */

function dusky_blog_get_default_breadcrumbs() {    
    return false;
}


function dusky_blog_get_default_sticky_menu() {    
    return false;
}


function dusky_blog_get_default_container_width() {    
    return 1400;
}