<?php

add_action( 'customize_register', 'dusky_blog_sticky_menu' );
function dusky_blog_sticky_menu( $wp_customize ) {

	$wp_customize->add_setting('dusky_blog_sticky_menu_option', array(
        'sanitize_callback'     =>  'dusky_blog_sanitize_checkbox',
        'default'               =>  dusky_blog_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'dusky_blog_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'dusky-blog'),
        'section' => 'dusky_blog_general_customization_section',
        'settings' => 'dusky_blog_sticky_menu_option',
        'type' => 'toggle',
    )));

}