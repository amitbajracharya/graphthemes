<?php


/* Default Site Title Color */
function dusky_blog_get_default_site_title_color() {
    return "#fdc57b";
}

/* Default Primary Color */
function dusky_blog_get_default_primary_color() {
    return "#cabca0";
}

/* Default Secondary Color */
function dusky_blog_get_default_secondary_color() {
    return "#9e586b";
}

/* Default Light Color */
function dusky_blog_get_default_light_color() {
    return "#f2f2f2";
}

/* Default Grey Color */
function dusky_blog_get_default_grey_color() {
    return "#7c8182";
}

/* Default Dark Color */
function dusky_blog_get_default_dark_color() {
    return "#33313b";
}

/* Default Background Color */
function dusky_blog_get_default_background_color() {
    return "33313b";
}