<div class="get-started-info">
	<h1><?php esc_html_e("Free v/s Pro", 'dusky-blog'); ?></h1>
	<table class="widefat">
		<thead><tr><th width="75%"></th><th><?php esc_html_e("Free", 'dusky-blog'); ?></th><th><?php esc_html_e("Pro", 'dusky-blog'); ?></th></tr></thead>
		<tbody>
			<tr><td><?php esc_html_e("1000+ Google Fonts", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Unlimited Color Option", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Footer Copyright", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("24/7 Support", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site Logo Management", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site width controller", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("In-built Social Sharing", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Pagination Options", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Child Theme Compatible", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Floating/Sticky Menu", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Header Background Image", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Blog Options", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Show/Hide Author Info", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Scroll to Top button", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Responsive Layout", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Live Customizer", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Theme Options", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Breadcrumb", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("SEO Optimized", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Lightweight", 'dusky-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>			
		<tbody>
		<tfoot>
			<tr><td></td><td colspan="2"><a style="float:right;" href="<?php echo esc_url( 'https://graphthemes.com/dusky-blog/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'Upgrade to Premium', 'dusky-blog' ); ?></a></td></tr>
		</tfoot>
	</table>
	
</div>
