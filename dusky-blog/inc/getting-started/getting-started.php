<?php
/**
 * Add a new page under Appearance
 */

function dusky_blog_getting_started_menu() {

	add_theme_page( esc_html__( 'Getting Started', 'dusky-blog' ), esc_html__( 'Getting Started', 'dusky-blog' ), 'edit_theme_options', 'dusky-blog-get-started', 'dusky_blog_getting_started_page' );
}
add_action( 'admin_menu', 'dusky_blog_getting_started_menu' );

/**
 * Enqueue styles for the help page.
 */
function dusky_blog_admin_scripts() {

	wp_enqueue_style( 'dusky-blog-admin-style', get_template_directory_uri() . '/inc/getting-started/getting-started.css', array(), DUSKY_BLOG_VERSION );
}
add_action( 'admin_enqueue_scripts', 'dusky_blog_admin_scripts' );

/**
 * Add the theme page
 */
function dusky_blog_getting_started_page() { ?>

<div class="main-info">

	<?php get_template_part( 'inc/getting-started/template-parts/main', 'info' ); ?>

</div>
<div class="top-wrapper">

	<?php get_template_part( 'inc/getting-started/template-parts/free-vs', 'pro' ); ?>

	<?php get_template_part( 'inc/getting-started/template-parts/faq' ); ?>


</div>
	<?php
}