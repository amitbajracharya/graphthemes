<?php


/* Default Site Title Color */
function buswick_get_default_site_title_color() {
    return "#000";
}

/* Default Primary Color */
function buswick_get_default_primary_color() {
    return "#0a0909";
}

/* Default Secondary Color */
function buswick_get_default_secondary_color() {
    return "#c83458";
}

/* Default Light Color */
function buswick_get_default_light_color() {
    return "#ffffff";
}

/* Default Grey Color */
function buswick_get_default_grey_color() {
    return "#969aa5";
}

/* Default Dark Color */
function buswick_get_default_dark_color() {
    return "#000000";
}

/* Default Background Color */
function buswick_get_default_background_color() {
    return "f4f4ed";
}