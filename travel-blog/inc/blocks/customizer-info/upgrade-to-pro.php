<?php

function buswick_customizer_upgrade_to_pro( $wp_customize ) {

	$wp_customize->add_section( new Buswick_Button_Control( $wp_customize, 'upgrade-to-pro',	array(
		'title'    => esc_html__( '★ Buswick Pro ', 'buswick' ),
		'type'	=> 'button',
		'pro_text' => esc_html__( 'Upgrade to Pro', 'buswick' ),
		'pro_url'  => esc_url( 'https://graphthemes.com/buswick/' ),
		'priority' => 1
	) )	);

	
}
add_action( 'customize_register', 'buswick_customizer_upgrade_to_pro' );


function buswick_enqueue_custom_admin_style() {
        wp_register_style( 'buswick-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.css', false );
        wp_enqueue_style( 'buswick-button' );

        wp_enqueue_script( 'buswick-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.js', array( 'jquery' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'buswick_enqueue_custom_admin_style' );