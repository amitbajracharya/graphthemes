<?php

/* Default Post Snippet Author*/
function buswick_get_default_post_snippet_author() {
    return false;
}

/* Default Post Snippet Category*/
function buswick_get_default_post_snippet_category() {
    return true;
}

/* Default Post Snippet Comment*/
function buswick_get_default_post_snippet_comment() {
    return false;
}

/* Default Post Snippet Date*/
function buswick_get_default_post_snippet_date() {
    return true;
}

/* Default Post Snippet Featured Image*/
function buswick_get_default_post_snippet_featured_image() {
    return true;
}

/* Default Post Snippet Tag*/
function buswick_get_default_post_snippet_featured_image_size() {
    return esc_html__( 'large', 'buswick' );
}

/* Default Post Snippet Tag*/
function buswick_get_default_post_snippet_tag() {
    return false;
}

/* Default Post Snippet Excerpt Szie  */
function buswick_get_default_post_snippet_excerpt_size() {
    return 15;
}

/* Default Post Snippet Read More */
function buswick_get_default_post_snippet_show_hide_read_more() {
    return true;
}

/* Default Post Snippet Read More Text */
function buswick_get_default_post_snippet_read_more_text() {
    return esc_html__( "Continue Reading", 'buswick' );
}

/* Default Post Snippet Social Share */
function buswick_get_default_post_snippet_social_share() {
    return true;
}

/* Default Post Snippet Social Share Options*/
function buswick_get_default_post_snippet_social_share_options() {
    return array();
}