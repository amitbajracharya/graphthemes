<?php

add_action( 'customize_register', 'buswick_breadcrumbs' );
function buswick_breadcrumbs( $wp_customize ) {

	$wp_customize->add_setting('buswick_breadcrumbs_option', array(
        'sanitize_callback'     =>  'buswick_sanitize_checkbox',
        'default'               =>  buswick_get_default_breadcrumbs(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'buswick_breadcrumbs_option', array(
        'label' => esc_html__('Enable Breadcrumbs', 'buswick'),
        'section' => 'buswick_general_customization_section',
        'settings' => 'buswick_breadcrumbs_option',
        'type' => 'toggle',
    )));

}