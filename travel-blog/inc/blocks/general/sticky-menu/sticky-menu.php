<?php

add_action( 'customize_register', 'buswick_sticky_menu' );
function buswick_sticky_menu( $wp_customize ) {

	$wp_customize->add_setting('buswick_sticky_menu_option', array(
        'sanitize_callback'     =>  'buswick_sanitize_checkbox',
        'default'               =>  buswick_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'buswick_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'buswick'),
        'section' => 'buswick_general_customization_section',
        'settings' => 'buswick_sticky_menu_option',
        'type' => 'toggle',
    )));

}