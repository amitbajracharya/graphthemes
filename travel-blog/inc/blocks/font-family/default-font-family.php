<?php

/* Default Font Family */
function buswick_get_default_site_identity_font_family() {    
    return esc_html__( "Playfair Display", 'buswick' );
}

function buswick_get_default_main_font_family() {    
    return esc_html__( "League Spartan", 'buswick' );
}

function buswick_get_default_secondary_font_family() {    
    return esc_html__( "Lora", 'buswick' );
}