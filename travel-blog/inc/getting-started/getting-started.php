<?php
/**
 * Add a new page under Appearance
 */

function buswick_getting_started_menu() {

	add_theme_page( esc_html__( 'Getting Started', 'buswick' ), esc_html__( 'Getting Started', 'buswick' ), 'edit_theme_options', 'buswick-get-started', 'buswick_getting_started_page' );
}
add_action( 'admin_menu', 'buswick_getting_started_menu' );

/**
 * Enqueue styles for the help page.
 */
function buswick_admin_scripts() {

	wp_enqueue_style( 'buswick-admin-style', get_template_directory_uri() . '/inc/getting-started/getting-started.css', array(), BUSWICK_VERSION );
}
add_action( 'admin_enqueue_scripts', 'buswick_admin_scripts' );

/**
 * Add the theme page
 */
function buswick_getting_started_page() { ?>

<div class="main-info">

	<?php get_template_part( 'inc/getting-started/template-parts/main', 'info' ); ?>

</div>
<div class="top-wrapper">

	<?php get_template_part( 'inc/getting-started/template-parts/free-vs', 'pro' ); ?>

	<?php get_template_part( 'inc/getting-started/template-parts/faq' ); ?>


</div>
	<?php
}