<?php

/* Default General Customization */

function buswick_get_default_breadcrumbs() {    
    return false;
}


function buswick_get_default_sticky_menu() {    
    return false;
}


function buswick_get_default_container_width() {    
    return 1300;
}