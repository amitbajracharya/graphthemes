<?php


/* Default Font Size */
function buswick_get_default_font_size() {
    return 18;
}

function buswick_get_default_logo_size() {
    return 60;
}


function buswick_get_default_site_identity_font_size() {
    return 30;
}


function buswick_get_default_font_weight() {
    return "400";
}

function buswick_get_default_line_height() {
    return "1.6";
}