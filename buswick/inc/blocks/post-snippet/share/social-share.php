<?php

add_action( 'customize_register', 'buswick_post_snippet_social_share' );
function buswick_post_snippet_social_share( $wp_customize ) {

	$wp_customize->add_setting( 'post_snippet_hide_show_social_share', array(
        'sanitize_callback'     =>  'buswick_sanitize_checkbox',
        'default'               =>  buswick_get_default_post_snippet_social_share()
    ) );

    $wp_customize->add_control( new Graphthemes_Toggle_Control( $wp_customize, 'post_snippet_hide_show_social_share', array(
        'label' => esc_html__( 'Enable Social Share','buswick' ),
        'section' => 'buswick_post_snippet_customization_section',
        'settings' => 'post_snippet_hide_show_social_share',
        'type'=> 'toggle',
    ) ) );

}




add_action( 'customize_register', 'buswick_post_snippet_social_share_options' );
function buswick_post_snippet_social_share_options( $wp_customize ) {

    $wp_customize->add_setting( 'post_snippet_social_share_options', array(
        'sanitize_callback' => 'buswick_sanitize_array',
        'default'     => buswick_get_default_post_snippet_social_share_options()
    ) );

    $wp_customize->add_control( new Graphthemes_Multi_Check_Control( $wp_customize, 'post_snippet_social_share_options', array(
        'label' => esc_html__( 'Social Shares', 'buswick' ),
        'section' => 'buswick_post_snippet_customization_section',
        'settings' => 'post_snippet_social_share_options',
        'type'=> 'multi-check',
        'choices'     => array(
            'facebook' => esc_html__( 'Facebook', 'buswick' ),
            'twitter' => esc_html__( 'Twitter', 'buswick' ),     
            'pinterest' => esc_html__( 'Pinterest', 'buswick' ),
            'linkedin' => esc_html__( 'LinkedIn', 'buswick' ),
            'email' => esc_html__( 'Email', 'buswick' ),
        ),
        'active_callback' => function() {
            return get_theme_mod( 'post_snippet_hide_show_social_share', buswick_get_default_post_snippet_social_share() );
        }
    ) ) );

    $wp_customize->add_setting( 'twitter_id', array(
        'sanitize_callback' =>  'wp_kses_post',
    ) );

    $wp_customize->add_control( 'twitter_id', array(
        'label' =>  esc_html__( 'Twitter ID:', 'buswick' ),
        'section'   =>  'buswick_post_snippet_customization_section',
        'Settings'  =>  'twitter_id',
        'type'=> 'text',
        'active_callback' => function() {
            return get_theme_mod( 'post_snippet_hide_show_social_share', buswick_get_default_post_snippet_social_share() );
        }
    ) );

}