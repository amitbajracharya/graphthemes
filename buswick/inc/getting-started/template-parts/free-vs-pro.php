<div class="get-started-info">
	<h1><?php esc_html_e("Free v/s Pro", 'buswick'); ?></h1>
	<table class="widefat">
		<thead><tr><th width="75%"></th><th><?php esc_html_e("Free", 'buswick'); ?></th><th><?php esc_html_e("Pro", 'buswick'); ?></th></tr></thead>
		<tbody>
			<tr><td><?php esc_html_e("1000+ Google Fonts", 'buswick'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Unlimited Color Option", 'buswick'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Footer Copyright", 'buswick'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("24/7 Support", 'buswick'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site Logo Management", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site width controller", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("In-built Social Sharing", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Pagination Options", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Child Theme Compatible", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Floating/Sticky Menu", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Header Background Image", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Blog Options", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Show/Hide Author Info", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Scroll to Top button", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Responsive Layout", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Live Customizer", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Theme Options", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Breadcrumb", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("SEO Optimized", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Lightweight", 'buswick'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>			
		<tbody>
		<tfoot>
			<tr><td></td><td colspan="2"><a style="float:right;" href="<?php echo esc_url( 'https://graphthemes.com/buswick/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'Upgrade to Premium', 'buswick' ); ?></a></td></tr>
		</tfoot>
	</table>
	
</div>
