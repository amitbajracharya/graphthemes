<?php

/* Default Font Family */
function whiz_get_default_site_identity_font_family()
{
    return esc_html__("Reem Kufi", 'whiz');
}

function whiz_get_default_main_font_family()
{
    return esc_html__("Poppins", 'whiz');
}

function whiz_get_default_secondary_font_family()
{
    return esc_html__("Cormorant", 'whiz');
}
