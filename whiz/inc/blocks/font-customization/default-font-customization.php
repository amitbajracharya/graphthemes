<?php


/* Default Font Size */
function whiz_get_default_font_size()
{
    return 15;
}

function whiz_get_default_logo_size()
{
    return 20;
}


function whiz_get_default_site_identity_font_size()
{
    return 30;
}


function whiz_get_default_font_weight()
{
    return "400";
}

function whiz_get_default_line_height()
{
    return "1.7";
}
