<?php

/* Default General Customization */

function whiz_get_default_breadcrumbs()
{
    return false;
}


function whiz_get_default_sticky_menu()
{
    return true;
}


function whiz_get_default_container_width()
{
    return 1400;
}
