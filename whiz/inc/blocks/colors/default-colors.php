<?php


/* Default Site Title Color */
function whiz_get_default_site_title_color()
{
    return "#3f7e88";
}

/* Default Primary Color */
function whiz_get_default_primary_color()
{
    return "#20c0e3";
}

/* Default Secondary Color */
function whiz_get_default_secondary_color()
{
    return "#3f7e88";
}

/* Default Light Color */
function whiz_get_default_light_color()
{
    return "#f5f7f9";
}

/* Default Grey Color */
function whiz_get_default_grey_color()
{
    return "#969aa5";
}

/* Default Dark Color */
function whiz_get_default_dark_color()
{
    return "#444444";
}

/* Default Background Color */
function whiz_get_default_background_color()
{
    return "ffffff";
}
