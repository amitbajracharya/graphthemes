<?php

/* Default Post Snippet Author*/
function whiz_get_default_post_snippet_author()
{
    return true;
}

/* Default Post Snippet Category*/
function whiz_get_default_post_snippet_category()
{
    return true;
}

/* Default Post Snippet Comment*/
function whiz_get_default_post_snippet_comment()
{
    return true;
}

/* Default Post Snippet Date*/
function whiz_get_default_post_snippet_date()
{
    return true;
}

/* Default Post Snippet Featured Image*/
function whiz_get_default_post_snippet_featured_image()
{
    return true;
}

/* Default Post Snippet Tag*/
function whiz_get_default_post_snippet_featured_image_size()
{
    return esc_html__('large', 'whiz');
}

/* Default Post Snippet Tag*/
function whiz_get_default_post_snippet_tag()
{
    return true;
}

/* Default Post Snippet Excerpt Szie  */
function whiz_get_default_post_snippet_excerpt_size()
{
    return 25;
}

/* Default Post Snippet Read More */
function whiz_get_default_post_snippet_show_hide_read_more()
{
    return true;
}

/* Default Post Snippet Read More Text */
function whiz_get_default_post_snippet_read_more_text()
{
    return esc_html__("Read More", 'whiz');
}

/* Default Post Snippet Social Share */
function whiz_get_default_post_snippet_social_share()
{
    return true;
}

/* Default Post Snippet Social Share Options*/
function whiz_get_default_post_snippet_social_share_options()
{
    return array();
}
