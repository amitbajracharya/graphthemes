<div class="get-started-info">
	<h1><?php esc_html_e("Free v/s Pro", 'whiz'); ?></h1>
	<table class="widefat">
		<thead>
			<tr>
				<th width="75%"></th>
				<th><?php esc_html_e("Free", 'whiz'); ?></th>
				<th><?php esc_html_e("Pro", 'whiz'); ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?php esc_html_e("1000+ Google Fonts", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-no"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Unlimited Color Option", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-no"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Footer Copyright", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-no"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("24/7 Support", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-no"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Site Logo Management", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Site width controller", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("In-built Social Sharing", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Pagination Options", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Child Theme Compatible", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Floating/Sticky Menu", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Header Background Image", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Blog Options", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Show/Hide Author Info", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Scroll to Top button", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Responsive Layout", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Live Customizer", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Theme Options", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Breadcrumb", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("SEO Optimized", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Lightweight", 'whiz'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
		<tbody>
		<tfoot>
			<tr>
				<td></td>
				<td colspan="2"><a style="float:right;" href="<?php echo esc_url('https://graphthemes.com/whiz/'); ?>" target="_blank" class="btn btn-success"><?php esc_html_e('Upgrade to Premium', 'whiz'); ?></a></td>
			</tr>
		</tfoot>
	</table>

</div>