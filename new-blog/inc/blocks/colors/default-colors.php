<?php


/* Default Site Title Color */
function glossy_blog_get_default_site_title_color() {
    return "#fff";
}

/* Default Primary Color */
function glossy_blog_get_default_primary_color() {
    return "#6ca1fc";
}

/* Default Secondary Color */
function glossy_blog_get_default_secondary_color() {
    return "#22ce85";
}

/* Default Light Color */
function glossy_blog_get_default_light_color() {
    return "#ffffff";
}

/* Default Grey Color */
function glossy_blog_get_default_grey_color() {
    return "#888888";
}

/* Default Dark Color */
function glossy_blog_get_default_dark_color() {
    return "#000000";
}

/* Default Background Color */
function glossy_blog_get_default_background_color() {
    return "9338c1";
}