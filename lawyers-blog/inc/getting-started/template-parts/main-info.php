<div class="main-info-detail">

	<h1><?php esc_html_e( 'Lawyers Blog', 'lawyers-blog' ); ?></h1>

	<p><?php esc_html_e( "Lawyers Blog is a professional and elegant WordPress theme designed for lawyers, law firms, attorneys, and legal consultants. It has a clean and modern layout that showcases your expertise and services in a captivating way. You can choose from different homepage layouts, blog styles, color schemes, and fonts to suit your branding and preferences. You can also use the powerful theme options panel to adjust various settings and features with just a few clicks. Lawyers Blog is fully responsive and mobile-friendly, meaning it adapts to any screen size and device. It is also SEO-optimized and fast-loading, ensuring that your website ranks well on search engines and delivers a smooth user experience. With Lawyers Blog, you can create a stunning and professional website for your law practice in no time. It is the best theme to start a blog related to lawyers because it offers everything you need to showcase your skills, services, and reputation. Get Lawyers Blog today and take your law business to the next level!", 'lawyers-blog' ); ?></p>
	
	<a class="btn btn-success" href="<?php echo esc_url( admin_url( '/customize.php' ) ); ?>">
		<?php esc_html_e( '⨳ Start Theme Customization', 'lawyers-blog' ); ?>
	</a>


	<?php esc_html_e("Need help?", 'lawyers-blog'); ?> <a href="<?php echo esc_url('https://graphthemes.com/support/'); ?>" target="_blank" class="link"><?php esc_html_e("Contact Us", 'lawyers-blog'); ?></a> <?php esc_html_e("or email us at: hello@graphthemes.com", 'lawyers-blog'); ?>

	<hr>
	<h2><?php esc_html_e( "Upgrade to Pro", 'lawyers-blog' ); ?></h2>
	<h4><?php esc_html_e("Need more color options, font options, edit footer copyright info?", 'lawyers-blog'); ?></h4>
	<a href="<?php echo esc_url( 'https://graphthemes.com/lawyers-blog' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'View Premium Version', 'lawyers-blog' ); ?></a>



</div>


<div>
	</div>