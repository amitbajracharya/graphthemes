<div class="das-faqs">
<h1><?php esc_html_e("FAQ's", 'lawyers-blog'); ?></h1>
<div class="das-wrap">
	<h3><?php esc_html_e("1. What is the difference between Free and Pro?", 'lawyers-blog'); ?></h3>
	<p><?php esc_html_e("Both Free and Premium version of the themes are well-built. If you are looking for more color options, font options, whitelabeling and more, then you can upgrade to premium.", 'lawyers-blog'); ?></p>
	<p><?php esc_html_e("The premium version lets you have better control over the theme as it comes with more customization options.", 'lawyers-blog'); ?> <a href="<?php echo esc_url( 'https://graphthemes.com/lawyers-blog' ); ?>" target="_blank"  class="link"><?php esc_html_e("here", 'lawyers-blog'); ?></a>.</p>
	<hr>
	<h3><?php esc_html_e("2. What are the advantages of upgrading to the Premium version?", 'lawyers-blog'); ?></h3>
	<p><?php esc_html_e("With Premium version, besides the extra features and frequent updates, you get premium support. If you run into any theme issues, you will get a lot quicker response compared to the free support.", 'lawyers-blog'); ?></p>
	<hr>
	<h3><?php esc_html_e("3. How do I change the copyright text?", 'lawyers-blog'); ?></h3>
	<p><?php esc_html_e("You can change the copyright text going to", 'lawyers-blog'); ?> <a href="<?php echo esc_url (admin_url( '/customize.php' ));?>" class="link"><?php esc_html_e("Appearance > Customize > Footer Copyright", 'lawyers-blog'); ?></a>. <?php esc_html_e("Upgrade to the", 'lawyers-blog'); ?> <a href="<?php echo esc_url( 'https://graphthemes.com/lawyers/' ); ?>" target="_blank"  class="link"><?php esc_html_e("Premium version", 'lawyers-blog'); ?></a> <?php esc_html_e("to unlock this feature.", 'lawyers-blog'); ?></p>
	<hr>

	<h2><?php esc_html_e("For more help, read our documentation", 'lawyers-blog'); ?></h2>
	<a href="<?php echo esc_url( 'https://graphthemes.com/theme-docs/lawyers-blog' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e("View Documentation", 'lawyers-blog'); ?></a>


	<div class="lawyers-panel">
		<div class="lawyers-panel-content">
			<div class="theme-title">
				<h4><?php esc_html_e( 'If you like our theme, please leave a review or Contact us for technical support.', 'lawyers-blog' ); ?></h4>
			</div>
			<a href="<?php echo esc_url( 'https://wordpress.org/support/theme/lawyers-blog/reviews/#new-post' ); ?>" target="_blank" class="btn btn-primary"><?php esc_html_e( 'Rate this theme', 'lawyers-blog' ); ?></a><a href="<?php echo esc_url( 'https://graphthemes.com/support/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'Contact Us', 'lawyers-blog' ); ?></a> 
		</div>
	</div>

</div>
</div>