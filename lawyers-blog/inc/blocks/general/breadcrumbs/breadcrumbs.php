<?php

add_action( 'customize_register', 'lawyers_blog_breadcrumbs' );
function lawyers_blog_breadcrumbs( $wp_customize ) {

	$wp_customize->add_setting('lawyers_blog_breadcrumbs_option', array(
        'sanitize_callback'     =>  'lawyers_blog_sanitize_checkbox',
        'default'               =>  lawyers_blog_get_default_breadcrumbs(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'lawyers_blog_breadcrumbs_option', array(
        'label' => esc_html__('Enable Breadcrumbs', 'lawyers-blog'),
        'section' => 'lawyers_blog_general_customization_section',
        'settings' => 'lawyers_blog_breadcrumbs_option',
        'type' => 'toggle',
    )));

}