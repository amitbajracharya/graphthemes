<?php

add_action( 'customize_register', 'lawyers_blog_sticky_menu' );
function lawyers_blog_sticky_menu( $wp_customize ) {

	$wp_customize->add_setting('lawyers_blog_sticky_menu_option', array(
        'sanitize_callback'     =>  'lawyers_blog_sanitize_checkbox',
        'default'               =>  lawyers_blog_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'lawyers_blog_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'lawyers-blog'),
        'section' => 'lawyers_blog_general_customization_section',
        'settings' => 'lawyers_blog_sticky_menu_option',
        'type' => 'toggle',
    )));

}