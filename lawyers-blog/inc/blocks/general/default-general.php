<?php

/* Default General Customization */

function lawyers_blog_get_default_breadcrumbs() {    
    return false;
}


function lawyers_blog_get_default_sticky_menu() {    
    return false;
}


function lawyers_blog_get_default_container_width() {    
    return 1400;
}