<?php


/* Default Font Size */
function lawyers_blog_get_default_font_size() {
    return 16;
}

function lawyers_blog_get_default_logo_size() {
    return 30;
}


function lawyers_blog_get_default_site_identity_font_size() {
    return 34;
}


function lawyers_blog_get_default_font_weight() {
    return "400";
}

function lawyers_blog_get_default_line_height() {
    return "1.7";
}