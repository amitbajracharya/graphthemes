<?php

add_action( 'customize_register', 'lawyers_blog_customizer_theme_info' );

function lawyers_blog_customizer_theme_info( $wp_customize ) {
	
    $wp_customize->add_section( 'lawyers_blog_theme_info_section' , array(
		'title'       => esc_html__( '❂ Theme Info' , 'lawyers-blog' ),
		'priority' => 2
	) );
    

	$wp_customize->add_setting( 'theme_info', array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
	) );
    
    $theme_info = '';
	
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Theme Details', 'lawyers-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/lawyers-blog/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'lawyers-blog' ) . '</a></span><hr>';

	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'How to use', 'lawyers-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/theme-docs/lawyers-blog/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'lawyers-blog' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'View Demo', 'lawyers-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/preview/?product_id=lawyers-blog' ) . '" target="_blank">' . esc_html__( 'Click Here', 'lawyers-blog' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Support Forum', 'lawyers-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://wordpress.org/support/theme/lawyers-blog' ) . '" target="_blank">' . esc_html__( 'Click Here', 'lawyers-blog' ) . '</a></span><hr>';

	$wp_customize->add_control( new Lawyers_Blog_Custom_Text( $wp_customize ,'theme_info',array(
		'section' => 'lawyers_blog_theme_info_section',
		'label' => $theme_info
	) ) );

}