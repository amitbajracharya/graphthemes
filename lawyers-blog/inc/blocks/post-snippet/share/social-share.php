<?php

add_action( 'customize_register', 'lawyers_blog_post_snippet_social_share' );
function lawyers_blog_post_snippet_social_share( $wp_customize ) {

	$wp_customize->add_setting( 'post_snippet_hide_show_social_share', array(
        'sanitize_callback'     =>  'lawyers_blog_sanitize_checkbox',
        'default'               =>  lawyers_blog_get_default_post_snippet_social_share()
    ) );

    $wp_customize->add_control( new Graphthemes_Toggle_Control( $wp_customize, 'post_snippet_hide_show_social_share', array(
        'label' => esc_html__( 'Enable Social Share','lawyers-blog' ),
        'section' => 'lawyers_blog_post_snippet_customization_section',
        'settings' => 'post_snippet_hide_show_social_share',
        'type'=> 'toggle',
    ) ) );

}




add_action( 'customize_register', 'lawyers_blog_post_snippet_social_share_options' );
function lawyers_blog_post_snippet_social_share_options( $wp_customize ) {

    $wp_customize->add_setting( 'post_snippet_social_share_options', array(
        'sanitize_callback' => 'lawyers_blog_sanitize_array',
        'default'     => lawyers_blog_get_default_post_snippet_social_share_options()
    ) );

    $wp_customize->add_control( new Graphthemes_Multi_Check_Control( $wp_customize, 'post_snippet_social_share_options', array(
        'label' => esc_html__( 'Social Shares', 'lawyers-blog' ),
        'section' => 'lawyers_blog_post_snippet_customization_section',
        'settings' => 'post_snippet_social_share_options',
        'type'=> 'multi-check',
        'choices'     => array(
            'facebook' => esc_html__( 'Facebook', 'lawyers-blog' ),
            'twitter' => esc_html__( 'Twitter', 'lawyers-blog' ),     
            'pinterest' => esc_html__( 'Pinterest', 'lawyers-blog' ),
            'linkedin' => esc_html__( 'LinkedIn', 'lawyers-blog' ),
            'email' => esc_html__( 'Email', 'lawyers-blog' ),
        ),
        'active_callback' => function() {
            return get_theme_mod( 'post_snippet_hide_show_social_share', lawyers_blog_get_default_post_snippet_social_share() );
        }
    ) ) );

    $wp_customize->add_setting( 'twitter_id', array(
        'sanitize_callback' =>  'wp_kses_post',
    ) );

    $wp_customize->add_control( 'twitter_id', array(
        'label' =>  esc_html__( 'Twitter ID:', 'lawyers-blog' ),
        'section'   =>  'lawyers_blog_post_snippet_customization_section',
        'Settings'  =>  'twitter_id',
        'type'=> 'text',
        'active_callback' => function() {
            return get_theme_mod( 'post_snippet_hide_show_social_share', lawyers_blog_get_default_post_snippet_social_share() );
        }
    ) );

}