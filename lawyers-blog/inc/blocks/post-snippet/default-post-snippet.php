<?php

/* Default Post Snippet Author*/
function lawyers_blog_get_default_post_snippet_author() {
    return true;
}

/* Default Post Snippet Category*/
function lawyers_blog_get_default_post_snippet_category() {
    return false;
}

/* Default Post Snippet Comment*/
function lawyers_blog_get_default_post_snippet_comment() {
    return true;
}

/* Default Post Snippet Date*/
function lawyers_blog_get_default_post_snippet_date() {
    return true;
}

/* Default Post Snippet Featured Image*/
function lawyers_blog_get_default_post_snippet_featured_image() {
    return true;
}

/* Default Post Snippet Tag*/
function lawyers_blog_get_default_post_snippet_featured_image_size() {
    return esc_html__( 'large', 'lawyers-blog' );
}

/* Default Post Snippet Tag*/
function lawyers_blog_get_default_post_snippet_tag() {
    return false;
}

/* Default Post Snippet Excerpt Szie  */
function lawyers_blog_get_default_post_snippet_excerpt_size() {
    return 15;
}

/* Default Post Snippet Read More */
function lawyers_blog_get_default_post_snippet_show_hide_read_more() {
    return true;
}

/* Default Post Snippet Read More Text */
function lawyers_blog_get_default_post_snippet_read_more_text() {
    return esc_html__( "Read More", 'lawyers-blog' );
}

/* Default Post Snippet Social Share */
function lawyers_blog_get_default_post_snippet_social_share() {
    return true;
}

/* Default Post Snippet Social Share Options*/
function lawyers_blog_get_default_post_snippet_social_share_options() {
    return array();
}