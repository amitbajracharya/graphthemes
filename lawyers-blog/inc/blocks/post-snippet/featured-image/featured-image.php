<?php


add_action( 'customize_register', 'lawyers_blog_post_snippet_featured_image' );
function lawyers_blog_post_snippet_featured_image( $wp_customize ) {

	$wp_customize->add_setting( 'post_snippet_hide_show_featured_image', array(
        'sanitize_callback'     =>  'lawyers_blog_sanitize_checkbox',
        'default'               =>  lawyers_blog_get_default_post_snippet_featured_image()
    ) );

    $wp_customize->add_control( new Graphthemes_Toggle_Control( $wp_customize, 'post_snippet_hide_show_featured_image', array(
        'label' => esc_html__( 'Show/Hide Featured Image','lawyers-blog' ),
        'section' => 'lawyers_blog_post_snippet_customization_section',
        'settings' => 'post_snippet_hide_show_featured_image',
        'type'=> 'toggle',
    ) ) );

}


add_action( 'customize_register', 'lawyers_blog_post_snippet_featured_image_size' );
function lawyers_blog_post_snippet_featured_image_size( $wp_customize ) {

    $wp_customize->add_setting( 'post_snippet_featured_image_size', array(
        'default'     => lawyers_blog_get_default_post_snippet_featured_image_size(),
        'sanitize_callback' => 'lawyers_blog_sanitize_select',
    ) );

    $wp_customize->add_control( 'post_snippet_featured_image_size', array(
        'settings'    => 'post_snippet_featured_image_size',
        'label'       =>  esc_html__( 'Post Thumbnail Options:', 'lawyers-blog' ),
        'section'     => 'lawyers_blog_post_snippet_customization_section',
        'type'        => 'select',
        'active_callback' => function(){
            return get_theme_mod( 'post_snippet_hide_show_featured_image', lawyers_blog_get_default_post_snippet_featured_image() );
        },
        'choices'     => array(
            'thumbnail' => esc_html__( 'Thumbnail', 'lawyers-blog' ),
            'medium' => esc_html__( 'Medium', 'lawyers-blog' ),
            'large' => esc_html__( 'Large', 'lawyers-blog' ),
            'full' => esc_html__( 'Full / Original', 'lawyers-blog' ),
        )
    ) );

}