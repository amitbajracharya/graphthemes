<?php


/* Default Site Title Color */
function lawyers_blog_get_default_site_title_color() {
    return "#b69d74";
}

/* Default Primary Color */
function lawyers_blog_get_default_primary_color() {
    return "#1f2839";
}

/* Default Secondary Color */
function lawyers_blog_get_default_secondary_color() {
    return "#b69d74";
}

/* Default Light Color */
function lawyers_blog_get_default_light_color() {
    return "#ffffff";
}

/* Default Grey Color */
function lawyers_blog_get_default_grey_color() {
    return "#969aa5";
}

/* Default Dark Color */
function lawyers_blog_get_default_dark_color() {
    return "#000000";
}

/* Default Background Color */
function lawyers_blog_get_default_background_color() {
    return "ffffff";
}