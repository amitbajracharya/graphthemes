<?php

/* Default Font Family */
function lawyers_blog_get_default_site_identity_font_family() {    
    return esc_html__( "Playfair Display", 'lawyers-blog' );
}

function lawyers_blog_get_default_main_font_family() {    
    return esc_html__( "Open Sans", 'lawyers-blog' );
}

function lawyers_blog_get_default_secondary_font_family() {    
    return esc_html__( "Playfair Display", 'lawyers-blog' );
}