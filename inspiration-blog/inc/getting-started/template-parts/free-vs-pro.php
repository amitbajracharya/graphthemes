<div class="get-started-info">
	<h1><?php esc_html_e("Free v/s Pro", 'inspiration-blog'); ?></h1>
	<table class="widefat">
		<thead><tr><th width="75%"></th><th><?php esc_html_e("Free", 'inspiration-blog'); ?></th><th><?php esc_html_e("Pro", 'inspiration-blog'); ?></th></tr></thead>
		<tbody>
			<tr><td><?php esc_html_e("1000+ Google Fonts", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Unlimited Color Option", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Footer Copyright", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("24/7 Support", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site Logo Management", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site width controller", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("In-built Social Sharing", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Pagination Options", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Child Theme Compatible", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Floating/Sticky Menu", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Header Background Image", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Blog Options", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Show/Hide Author Info", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Scroll to Top button", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Responsive Layout", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Live Customizer", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Theme Options", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Breadcrumb", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("SEO Optimized", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Lightweight", 'inspiration-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>			
		<tbody>
		<tfoot>
			<tr><td></td><td colspan="2"><a style="float:right;" href="<?php echo esc_url( 'https://graphthemes.com/inspiration-blog/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'Upgrade to Premium', 'inspiration-blog' ); ?></a></td></tr>
		</tfoot>
	</table>
	
</div>
