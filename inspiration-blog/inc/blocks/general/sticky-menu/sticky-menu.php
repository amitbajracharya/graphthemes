<?php

add_action( 'customize_register', 'inspiration_blog_sticky_menu' );
function inspiration_blog_sticky_menu( $wp_customize ) {

	$wp_customize->add_setting('inspiration_blog_sticky_menu_option', array(
        'sanitize_callback'     =>  'inspiration_blog_sanitize_checkbox',
        'default'               =>  inspiration_blog_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'inspiration_blog_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'inspiration-blog'),
        'section' => 'inspiration_blog_general_customization_section',
        'settings' => 'inspiration_blog_sticky_menu_option',
        'type' => 'toggle',
    )));

}