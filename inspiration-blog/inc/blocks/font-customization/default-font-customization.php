<?php


/* Default Font Size */
function inspiration_blog_get_default_font_size() {
    return 16;
}

function inspiration_blog_get_default_logo_size() {
    return 60;
}


function inspiration_blog_get_default_site_identity_font_size() {
    return 30;
}


function inspiration_blog_get_default_font_weight() {
    return "400";
}

function inspiration_blog_get_default_line_height() {
    return "1.6";
}