<?php

/* Default Post Snippet Author*/
function inspiration_blog_get_default_post_snippet_author() {
    return true;
}

/* Default Post Snippet Category*/
function inspiration_blog_get_default_post_snippet_category() {
    return false;
}

/* Default Post Snippet Comment*/
function inspiration_blog_get_default_post_snippet_comment() {
    return false;
}

/* Default Post Snippet Date*/
function inspiration_blog_get_default_post_snippet_date() {
    return true;
}

/* Default Post Snippet Featured Image*/
function inspiration_blog_get_default_post_snippet_featured_image() {
    return true;
}

/* Default Post Snippet Tag*/
function inspiration_blog_get_default_post_snippet_featured_image_size() {
    return esc_html__( 'large', 'inspiration-blog' );
}

/* Default Post Snippet Tag*/
function inspiration_blog_get_default_post_snippet_tag() {
    return false;
}

/* Default Post Snippet Excerpt Szie  */
function inspiration_blog_get_default_post_snippet_excerpt_size() {
    return 15;
}

/* Default Post Snippet Read More */
function inspiration_blog_get_default_post_snippet_show_hide_read_more() {
    return true;
}

/* Default Post Snippet Read More Text */
function inspiration_blog_get_default_post_snippet_read_more_text() {
    return esc_html__( "Read More", 'inspiration-blog' );
}

/* Default Post Snippet Social Share */
function inspiration_blog_get_default_post_snippet_social_share() {
    return true;
}

/* Default Post Snippet Social Share Options*/
function inspiration_blog_get_default_post_snippet_social_share_options() {
    return array();
}