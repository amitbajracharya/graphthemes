<?php

/* Default Post Detail Author*/
function inspiration_blog_get_default_post_detail_author() {
    return true;
}

/* Default Post Detail Category*/
function inspiration_blog_get_default_post_detail_category() {
    return true;
}

/* Default Post Detail Comment*/
function inspiration_blog_get_default_post_detail_comment() {
    return true;
}

/* Default Post Detail Date*/
function inspiration_blog_get_default_post_detail_date() {
    return true;
}

/* Default Post Detail Featured Image*/
function inspiration_blog_get_default_post_detail_featured_image() {
    return true;
}

/* Default Post Detail Tag*/
function inspiration_blog_get_default_post_detail_featured_image_size() {
    return esc_html__( 'large', 'inspiration-blog' );
}

/* Default Post Detail Tag*/
function inspiration_blog_get_default_post_detail_tag() {
    return true;
}

/* Default Post Detail Social Share */
function inspiration_blog_get_default_post_detail_social_share() {
    return true;
}

/* Default Post Detail Social Share Options*/
function inspiration_blog_get_default_post_detail_social_share_options() {
    return array();
}

/* Default Post Detail Author Block Options */
function inspiration_blog_get_default_post_detail_author_block() {
    return true;
}

/* Default Post Detail Related Articles Options*/
function inspiration_blog_get_default_post_detail_related_articles() {
    return true;
}

/* Default Post Detail Related Articles Title */
function inspiration_blog_get_default_post_detail_related_articles_title() {
    return esc_html__( "Related Articles", 'inspiration-blog' );
}