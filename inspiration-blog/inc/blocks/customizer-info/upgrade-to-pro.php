<?php

function inspiration_blog_customizer_upgrade_to_pro( $wp_customize ) {

	$wp_customize->add_section( new Inspiration_Blog_Button_Control( $wp_customize, 'upgrade-to-pro',	array(
		'title'    => esc_html__( '★ Inspiration Blog Pro ', 'inspiration-blog' ),
		'type'	=> 'button',
		'pro_text' => esc_html__( 'Upgrade to Pro', 'inspiration-blog' ),
		'pro_url'  => esc_url( 'https://graphthemes.com/inspiration-blog/' ),
		'priority' => 1
	) )	);

	
}
add_action( 'customize_register', 'inspiration_blog_customizer_upgrade_to_pro' );


function inspiration_blog_enqueue_custom_admin_style() {
        wp_register_style( 'inspiration-blog-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.css', false );
        wp_enqueue_style( 'inspiration-blog-button' );

        wp_enqueue_script( 'inspiration-blog-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.js', array( 'jquery' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'inspiration_blog_enqueue_custom_admin_style' );