<?php

add_action( 'customize_register', 'inspiration_blog_customizer_theme_info' );

function inspiration_blog_customizer_theme_info( $wp_customize ) {
	
    $wp_customize->add_section( 'inspiration_blog_theme_info_section' , array(
		'title'       => esc_html__( '❂ Theme Info' , 'inspiration-blog' ),
		'priority' => 2
	) );
    

	$wp_customize->add_setting( 'theme_info', array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
	) );
    
    $theme_info = '';
	
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Theme Details', 'inspiration-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/inspiration-blog/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'inspiration-blog' ) . '</a></span><hr>';

	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'How to use', 'inspiration-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/theme-docs/inspiration-blog/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'inspiration-blog' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'View Demo', 'inspiration-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/preview/?product_id=inspiration-blog' ) . '" target="_blank">' . esc_html__( 'Click Here', 'inspiration-blog' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Support Forum', 'inspiration-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://wordpress.org/support/theme/inspiration-blog' ) . '" target="_blank">' . esc_html__( 'Click Here', 'inspiration-blog' ) . '</a></span><hr>';

	$wp_customize->add_control( new Inspiration_Blog_Custom_Text( $wp_customize ,'theme_info',array(
		'section' => 'inspiration_blog_theme_info_section',
		'label' => $theme_info
	) ) );

}