<?php

/* Default Font Family */
function inspiration_blog_get_default_site_identity_font_family() {    
    return esc_html__( "Syne", 'inspiration-blog' );
}

function inspiration_blog_get_default_main_font_family() {    
    return esc_html__( "Noto Serif Display", 'inspiration-blog' );
}

function inspiration_blog_get_default_secondary_font_family() {    
    return esc_html__( "Oxygen", 'inspiration-blog' );
}