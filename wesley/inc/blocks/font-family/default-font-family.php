<?php

/* Default Font Family */
function wesley_get_default_site_identity_font_family() {    
    return esc_html__( "Playfair Display", 'wesley' );
}

function wesley_get_default_main_font_family() {    
    return esc_html__( "lato", 'wesley' );
}

function wesley_get_default_secondary_font_family() {    
    return esc_html__( "playfair display", 'wesley' );
}