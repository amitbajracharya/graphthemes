<?php

add_action( 'customize_register', 'wesley_breadcrumbs' );
function wesley_breadcrumbs( $wp_customize ) {

	$wp_customize->add_setting('wesley_breadcrumbs_option', array(
        'sanitize_callback'     =>  'wesley_sanitize_checkbox',
        'default'               =>  wesley_get_default_breadcrumbs(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'wesley_breadcrumbs_option', array(
        'label' => esc_html__('Enable Breadcrumbs', 'wesley'),
        'section' => 'wesley_general_customization_section',
        'settings' => 'wesley_breadcrumbs_option',
        'type' => 'toggle',
    )));

}