<?php

/* Default General Customization */

function wesley_get_default_breadcrumbs() {    
    return false;
}


function wesley_get_default_sticky_menu() {    
    return false;
}


function wesley_get_default_container_width() {    
    return 1400;
}