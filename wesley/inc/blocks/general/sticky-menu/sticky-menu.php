<?php

add_action( 'customize_register', 'wesley_sticky_menu' );
function wesley_sticky_menu( $wp_customize ) {

	$wp_customize->add_setting('wesley_sticky_menu_option', array(
        'sanitize_callback'     =>  'wesley_sanitize_checkbox',
        'default'               =>  wesley_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'wesley_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'wesley'),
        'section' => 'wesley_general_customization_section',
        'settings' => 'wesley_sticky_menu_option',
        'type' => 'toggle',
    )));

}