<?php

/* Default Post Snippet Author*/
function wesley_get_default_post_snippet_author() {
    return true;
}

/* Default Post Snippet Category*/
function wesley_get_default_post_snippet_category() {
    return true;
}

/* Default Post Snippet Comment*/
function wesley_get_default_post_snippet_comment() {
    return true;
}

/* Default Post Snippet Date*/
function wesley_get_default_post_snippet_date() {
    return true;
}

/* Default Post Snippet Featured Image*/
function wesley_get_default_post_snippet_featured_image() {
    return true;
}

/* Default Post Snippet Tag*/
function wesley_get_default_post_snippet_featured_image_size() {
    return esc_html__( 'large', 'wesley' );
}

/* Default Post Snippet Tag*/
function wesley_get_default_post_snippet_tag() {
    return true;
}

/* Default Post Snippet Excerpt Szie  */
function wesley_get_default_post_snippet_excerpt_size() {
    return 25;
}

/* Default Post Snippet Read More */
function wesley_get_default_post_snippet_show_hide_read_more() {
    return true;
}

/* Default Post Snippet Read More Text */
function wesley_get_default_post_snippet_read_more_text() {
    return esc_html__( "Read More", 'wesley' );
}

/* Default Post Snippet Social Share */
function wesley_get_default_post_snippet_social_share() {
    return true;
}

/* Default Post Snippet Social Share Options*/
function wesley_get_default_post_snippet_social_share_options() {
    return array();
}