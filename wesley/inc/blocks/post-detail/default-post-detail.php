<?php

/* Default Post Detail Author*/
function wesley_get_default_post_detail_author() {
    return true;
}

/* Default Post Detail Category*/
function wesley_get_default_post_detail_category() {
    return true;
}

/* Default Post Detail Comment*/
function wesley_get_default_post_detail_comment() {
    return true;
}

/* Default Post Detail Date*/
function wesley_get_default_post_detail_date() {
    return true;
}

/* Default Post Detail Featured Image*/
function wesley_get_default_post_detail_featured_image() {
    return true;
}

/* Default Post Detail Tag*/
function wesley_get_default_post_detail_featured_image_size() {
    return esc_html__( 'large', 'wesley' );
}

/* Default Post Detail Tag*/
function wesley_get_default_post_detail_tag() {
    return true;
}

/* Default Post Detail Social Share */
function wesley_get_default_post_detail_social_share() {
    return true;
}

/* Default Post Detail Social Share Options*/
function wesley_get_default_post_detail_social_share_options() {
    return array();
}

/* Default Post Detail Author Block Options */
function wesley_get_default_post_detail_author_block() {
    return true;
}

/* Default Post Detail Related Articles Options*/
function wesley_get_default_post_detail_related_articles() {
    return true;
}

/* Default Post Detail Related Articles Title */
function wesley_get_default_post_detail_related_articles_title() {
    return esc_html__( "Related Articles", 'wesley' );
}