<?php


/* Default Site Title Color */
function wesley_get_default_site_title_color() {
    return "#000";
}

/* Default Primary Color */
function wesley_get_default_primary_color() {
    return "#000000";
}

/* Default Secondary Color */
function wesley_get_default_secondary_color() {
    return "#c27259";
}

/* Default Light Color */
function wesley_get_default_light_color() {
    return "#ffffff";
}

/* Default Grey Color */
function wesley_get_default_grey_color() {
    return "#969aa5";
}

/* Default Dark Color */
function wesley_get_default_dark_color() {
    return "#000000";
}

/* Default Background Color */
function wesley_get_default_background_color() {
    return "f8ead7";
}