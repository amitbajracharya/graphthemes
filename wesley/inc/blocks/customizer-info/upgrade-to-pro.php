<?php

function wesley_customizer_upgrade_to_pro( $wp_customize ) {

	$wp_customize->add_section( new Wesley_Button_Control( $wp_customize, 'upgrade-to-pro',	array(
		'title'    => esc_html__( '★ Wesley Pro ', 'wesley' ),
		'type'	=> 'button',
		'pro_text' => esc_html__( 'Upgrade to Pro', 'wesley' ),
		'pro_url'  => esc_url( 'https://graphthemes.com/wesley/' ),
		'priority' => 1
	) )	);

	
}
add_action( 'customize_register', 'wesley_customizer_upgrade_to_pro' );


function wesley_enqueue_custom_admin_style() {
        wp_register_style( 'wesley-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.css', false );
        wp_enqueue_style( 'wesley-button' );

        wp_enqueue_script( 'wesley-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.js', array( 'jquery' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'wesley_enqueue_custom_admin_style' );