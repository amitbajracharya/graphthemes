<?php

add_action( 'customize_register', 'wesley_customizer_theme_info' );

function wesley_customizer_theme_info( $wp_customize ) {
	
    $wp_customize->add_section( 'wesley_theme_info_section' , array(
		'title'       => esc_html__( '❂ Theme Info' , 'wesley' ),
		'priority' => 2
	) );
    

	$wp_customize->add_setting( 'theme_info', array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
	) );
    
    $theme_info = '';
	
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Theme Details', 'wesley' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/wesley/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'wesley' ) . '</a></span><hr>';

	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'How to use', 'wesley' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/theme-docs/wesley/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'wesley' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'View Demo', 'wesley' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/preview/?product_id=wesley' ) . '" target="_blank">' . esc_html__( 'Click Here', 'wesley' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Support Forum', 'wesley' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://wordpress.org/support/theme/wesley' ) . '" target="_blank">' . esc_html__( 'Click Here', 'wesley' ) . '</a></span><hr>';

	$wp_customize->add_control( new Wesley_Custom_Text( $wp_customize ,'theme_info',array(
		'section' => 'wesley_theme_info_section',
		'label' => $theme_info
	) ) );

}