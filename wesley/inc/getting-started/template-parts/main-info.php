<div class="main-info-detail">

	<h1><?php esc_html_e( 'Wesley', 'wesley' ); ?></h1>

	<p><?php esc_html_e( "The ultimate WP theme for lifestyle bloggers! Say goodbye to clunky and outdated themes, and hello to a sleek and stylish design that will make your blog stand out from the crowd. With a responsive and modern design, Wesley will look stunning on all devices, ensuring that your readers can enjoy your content no matter where they are. Wesley is easy to use and comes packed with all the features you need to create a truly remarkable blog. From custom post types to unlimited color options, Wesley has everything you need to create a unique and professional-looking blog. Whether you're a seasoned blogger or just starting out, Wesley's intuitive interface makes it easy to create and manage your content. So why wait? Take your lifestyle blog to the next level with Wesley, the perfect WordPress theme for bloggers who want to make a statement. Get Wesley today and start sharing your story with the world!", 'wesley' ); ?></p>
	
	<a class="btn btn-success" href="<?php echo esc_url( admin_url( '/customize.php' ) ); ?>">
		<?php esc_html_e( '⨳ Start Theme Customization', 'wesley' ); ?>
	</a>


	<?php esc_html_e("Need help?", 'wesley'); ?> <a href="<?php echo esc_url('https://graphthemes.com/support/'); ?>" target="_blank" class="link"><?php esc_html_e("Contact Us", 'wesley'); ?></a> <?php esc_html_e("or email us at: hello@graphthemes.com", 'wesley'); ?>

	<hr>
	<h2><?php esc_html_e( "Upgrade to Pro", 'wesley' ); ?></h2>
	<h4><?php esc_html_e("Need more color options, font options, edit footer copyright info?", 'wesley'); ?></h4>
	<a href="<?php echo esc_url( 'https://graphthemes.com/wesley/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'View Premium Version', 'wesley' ); ?></a>



</div>


<div>
	</div>