<?php
/**
 * Add a new page under Appearance
 */

function wesley_getting_started_menu() {

	add_theme_page( esc_html__( 'Getting Started', 'wesley' ), esc_html__( 'Getting Started', 'wesley' ), 'edit_theme_options', 'wesley-get-started', 'wesley_getting_started_page' );
}
add_action( 'admin_menu', 'wesley_getting_started_menu' );

/**
 * Enqueue styles for the help page.
 */
function wesley_admin_scripts() {

	wp_enqueue_style( 'wesley-admin-style', get_template_directory_uri() . '/inc/getting-started/getting-started.css', array(), WESLEY_VERSION );
}
add_action( 'admin_enqueue_scripts', 'wesley_admin_scripts' );

/**
 * Add the theme page
 */
function wesley_getting_started_page() { ?>

<div class="main-info">

	<?php get_template_part( 'inc/getting-started/template-parts/main', 'info' ); ?>

</div>
<div class="top-wrapper">

	<?php get_template_part( 'inc/getting-started/template-parts/free-vs', 'pro' ); ?>

	<?php get_template_part( 'inc/getting-started/template-parts/faq' ); ?>


</div>
	<?php
}