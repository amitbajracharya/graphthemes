<?php

add_action( 'customize_register', 'workout_blog_customizer_theme_info' );

function workout_blog_customizer_theme_info( $wp_customize ) {
	
    $wp_customize->add_section( 'workout_blog_theme_info_section' , array(
		'title'       => esc_html__( '❂ Theme Info' , 'workout-blog' ),
		'priority' => 2
	) );
    

	$wp_customize->add_setting( 'theme_info', array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
	) );
    
    $theme_info = '';
	
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Theme Details', 'workout-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/workout-blog/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'workout-blog' ) . '</a></span><hr>';

	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'How to use', 'workout-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/theme-docs/workout-blog/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'workout-blog' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'View Demo', 'workout-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/preview/?product_id=workout-blog' ) . '" target="_blank">' . esc_html__( 'Click Here', 'workout-blog' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Support Forum', 'workout-blog' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://wordpress.org/support/theme/workout-blog' ) . '" target="_blank">' . esc_html__( 'Click Here', 'workout-blog' ) . '</a></span><hr>';

	$wp_customize->add_control( new Workout_Blog_Custom_Text( $wp_customize ,'theme_info',array(
		'section' => 'workout_blog_theme_info_section',
		'label' => $theme_info
	) ) );

}