<?php

function workout_blog_customizer_upgrade_to_pro( $wp_customize ) {

	$wp_customize->add_section( new Workout_Blog_Button_Control( $wp_customize, 'upgrade-to-pro',	array(
		'title'    => esc_html__( '★ Lawyers Pro ', 'workout-blog' ),
		'type'	=> 'button',
		'pro_text' => esc_html__( 'Upgrade to Pro', 'workout-blog' ),
		'pro_url'  => esc_url( 'https://graphthemes.com/lawyers/' ),
		'priority' => 1
	) )	);

	
}
add_action( 'customize_register', 'workout_blog_customizer_upgrade_to_pro' );


function workout_blog_enqueue_custom_admin_style() {
        wp_register_style( 'lawyers-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.css', false );
        wp_enqueue_style( 'lawyers-button' );

        wp_enqueue_script( 'lawyers-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.js', array( 'jquery' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'workout_blog_enqueue_custom_admin_style' );