<?php


/* Default Site Title Color */
function workout_blog_get_default_site_title_color() {
    return "#ffffff";
}

/* Default Primary Color */
function workout_blog_get_default_primary_color() {
    return "#000000";
}

/* Default Secondary Color */
function workout_blog_get_default_secondary_color() {
    return "#eefb13";
}

/* Default Light Color */
function workout_blog_get_default_light_color() {
    return "#ffffff";
}

/* Default Grey Color */
function workout_blog_get_default_grey_color() {
    return "#bbbbbb";
}

/* Default Dark Color */
function workout_blog_get_default_dark_color() {
    return "#000000";
}

/* Default Background Color */
function workout_blog_get_default_background_color() {
    return "000000";
}