<?php

add_action( 'customize_register', 'workout_blog_post_snippet_date' );
function workout_blog_post_snippet_date( $wp_customize ) {

	$wp_customize->add_setting( 'post_snippet_hide_show_date', array(
        'sanitize_callback'     =>  'workout_blog_sanitize_checkbox',
        'default'               =>  workout_blog_get_default_post_snippet_date()
    ) );

    $wp_customize->add_control( new Graphthemes_Toggle_Control( $wp_customize, 'post_snippet_hide_show_date', array(
        'label' => esc_html__( 'Show/Hide Date','workout-blog' ),
        'section' => 'workout_blog_post_snippet_customization_section',
        'settings' => 'post_snippet_hide_show_date',
        'type'=> 'toggle',
    ) ) );

}