<?php

/* Default Post Snippet Author*/
function workout_blog_get_default_post_snippet_author() {
    return false;
}

/* Default Post Snippet Category*/
function workout_blog_get_default_post_snippet_category() {
    return false;
}

/* Default Post Snippet Comment*/
function workout_blog_get_default_post_snippet_comment() {
    return false;
}

/* Default Post Snippet Date*/
function workout_blog_get_default_post_snippet_date() {
    return false;
}

/* Default Post Snippet Featured Image*/
function workout_blog_get_default_post_snippet_featured_image() {
    return true;
}

/* Default Post Snippet Tag*/
function workout_blog_get_default_post_snippet_featured_image_size() {
    return esc_html__( 'large', 'workout-blog' );
}

/* Default Post Snippet Tag*/
function workout_blog_get_default_post_snippet_tag() {
    return false;
}

/* Default Post Snippet Excerpt Szie  */
function workout_blog_get_default_post_snippet_excerpt_size() {
    return 15;
}

/* Default Post Snippet Read More */
function workout_blog_get_default_post_snippet_show_hide_read_more() {
    return true;
}

/* Default Post Snippet Read More Text */
function workout_blog_get_default_post_snippet_read_more_text() {
    return esc_html__( "Read More", 'workout-blog' );
}

/* Default Post Snippet Social Share */
function workout_blog_get_default_post_snippet_social_share() {
    return true;
}

/* Default Post Snippet Social Share Options*/
function workout_blog_get_default_post_snippet_social_share_options() {
    return array();
}