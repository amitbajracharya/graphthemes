<?php

/* Default Font Family */
function workout_blog_get_default_site_identity_font_family() {    
    return esc_html__( "Vazirmatn", 'workout-blog' );
}

function workout_blog_get_default_main_font_family() {    
    return esc_html__( "Vazirmatn", 'workout-blog' );
}

function workout_blog_get_default_secondary_font_family() {    
    return esc_html__( "Vazirmatn", 'workout-blog' );
}