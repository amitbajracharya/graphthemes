<?php

add_action( 'customize_register', 'workout_blog_main_font_family' );
function workout_blog_main_font_family( $wp_customize ) {

    $wp_customize->add_setting( 'main_font_family', array(
        'default'     => workout_blog_get_default_main_font_family(),
        'transport'   => 'postMessage',
        'sanitize_callback' => 'workout_blog_sanitize_google_fonts'
    ) );

    $wp_customize->add_control( 'main_font_family', array(
        'settings'    => 'main_font_family',
        'label'       =>  esc_html__( 'Primary Font', 'workout-blog' ),
        'section'     => 'workout_blog_font_customization_section',
        'type'        => 'select',
        'choices'     => workout_blog_google_fonts( workout_blog_free_pro() ),
    ) );

}


add_action( 'customize_preview_init', 'workout_blog_main_font_family_enqueue_scripts' );
function workout_blog_main_font_family_enqueue_scripts() {

    $main_font_family = esc_attr( get_theme_mod( 'main_font_family', workout_blog_get_default_main_font_family() ) );

    wp_enqueue_script( 'graphthemes-main-font-family-customizer', get_template_directory_uri() . '/inc/blocks/font-family/main/customizer-main-font-family.js', array('jquery'), '', true );
}


add_action( 'wp_enqueue_scripts', 'workout_blog_main_font_family_dynamic_css' );
function workout_blog_main_font_family_dynamic_css() {

    $main_font_family = esc_attr( get_theme_mod( 'main_font_family', workout_blog_get_default_main_font_family() ) );

    $dynamic_css = ":root { --primary-font: $main_font_family; }";

    wp_add_inline_style( 'workout-blog', $dynamic_css );
}