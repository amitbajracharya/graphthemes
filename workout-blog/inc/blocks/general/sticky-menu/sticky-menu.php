<?php

add_action( 'customize_register', 'workout_blog_sticky_menu' );
function workout_blog_sticky_menu( $wp_customize ) {

	$wp_customize->add_setting('workout_blog_sticky_menu_option', array(
        'sanitize_callback'     =>  'workout_blog_sanitize_checkbox',
        'default'               =>  workout_blog_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'workout_blog_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'workout-blog'),
        'section' => 'workout_blog_general_customization_section',
        'settings' => 'workout_blog_sticky_menu_option',
        'type' => 'toggle',
    )));

}