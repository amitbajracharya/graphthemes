<?php

/* Default General Customization */

function workout_blog_get_default_breadcrumbs() {    
    return false;
}


function workout_blog_get_default_sticky_menu() {    
    return false;
}


function workout_blog_get_default_container_width() {    
    return 1400;
}