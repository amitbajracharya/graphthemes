<div class="main-info-detail">

	<h1><?php esc_html_e( 'Workout Blog', 'workout-blog' ); ?></h1>

	<p><?php esc_html_e( "Workout Blog is a stunning WordPress theme designed for workout enthusiasts, personal trainers, and gym owners. It features a modern and sleek layout that showcases your content in an engaging and attractive way. Whether you want to share your workout tips, nutrition advice, or product reviews, Workout Blog has everything you need to create a professional and captivating fitness website. With Workout Blog, you can easily customize your site with unlimited colors, fonts, and layouts. You can also use the built-in page builder to create unique and dynamic pages with drag-and-drop elements. Plus, Workout Blog is fully responsive and SEO-friendly, so your site will look great and rank well on any device and search engine. Workout Blog is the perfect WordPress theme for anyone who wants to start or grow their fitness blog. It comes with a detailed documentation, and dedicated support to help you get started in minutes. Don't miss this opportunity to create a fitness blog that stands out from the crowd with Workout Blog WordPress theme.", 'workout-blog' ); ?></p>
	
	<a class="btn btn-success" href="<?php echo esc_url( admin_url( '/customize.php' ) ); ?>">
		<?php esc_html_e( '⨳ Start Theme Customization', 'workout-blog' ); ?>
	</a>


	<?php esc_html_e("Need help?", 'workout-blog'); ?> <a href="<?php echo esc_url('https://graphthemes.com/support/'); ?>" target="_blank" class="link"><?php esc_html_e("Contact Us", 'workout-blog'); ?></a> <?php esc_html_e("or email us at: hello@graphthemes.com", 'workout-blog'); ?>

	<hr>
	<h2><?php esc_html_e( "Upgrade to Pro", 'workout-blog' ); ?></h2>
	<h4><?php esc_html_e("Need more color options, font options, edit footer copyright info?", 'workout-blog'); ?></h4>
	<a href="<?php echo esc_url( 'https://graphthemes.com/workout-blog' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'View Premium Version', 'workout-blog' ); ?></a>



</div>


<div>
	</div>