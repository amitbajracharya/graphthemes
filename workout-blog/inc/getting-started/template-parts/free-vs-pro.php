<div class="get-started-info">
	<h1><?php esc_html_e("Free v/s Pro", 'workout-blog'); ?></h1>
	<table class="widefat">
		<thead><tr><th width="75%"></th><th><?php esc_html_e("Free", 'workout-blog'); ?></th><th><?php esc_html_e("Pro", 'workout-blog'); ?></th></tr></thead>
		<tbody>
			<tr><td><?php esc_html_e("1000+ Google Fonts", 'workout-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Unlimited Color Option", 'workout-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Footer Copyright", 'workout-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("24/7 Support", 'workout-blog'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site Logo Management", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site width controller", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("In-built Social Sharing", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Pagination Options", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Child Theme Compatible", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Floating/Sticky Menu", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Header Background Image", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Blog Options", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Show/Hide Author Info", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Scroll to Top button", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Responsive Layout", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Live Customizer", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Theme Options", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Breadcrumb", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("SEO Optimized", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Lightweight", 'workout-blog'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>			
		<tbody>
		<tfoot>
			<tr><td></td><td colspan="2"><a style="float:right;" href="<?php echo esc_url( 'https://graphthemes.com/workout-blog' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'Upgrade to Premium', 'workout-blog' ); ?></a></td></tr>
		</tfoot>
	</table>
	
</div>
