<div class="main-info-detail">

	<h1><?php esc_html_e( 'Plush', 'plush' ); ?></h1>

	<p><?php esc_html_e( 'An easy to use WordPress theme for Passionate Bloggers, Writers, Editors and more.  This theme is built with hassle-free and powerful customization options that allow users to easily customize without writing a single line of code. You can try customizing theme now without any risk.', 'plush' ); ?></p>
	
	<a class="btn btn-success" href="<?php echo esc_url( admin_url( '/customize.php' ) ); ?>">
		<?php esc_html_e( '⨳ Start Theme Customization', 'plush' ); ?>
	</a>


	<?php esc_html_e("Need help?", 'plush'); ?> <a href="<?php echo esc_url('https://graphthemes.com/support/'); ?>" target="_blank" class="link"><?php esc_html_e("Contact Us", 'plush'); ?></a> <?php esc_html_e("or email us at: hello@graphthemes.com", 'plush'); ?>

	<hr>
	<h2><?php esc_html_e( "Upgrade to Pro", 'plush' ); ?></h2>
	<h4><?php esc_html_e("Need more color options, font options, edit footer copyright info?", 'plush'); ?></h4>
	<a href="<?php echo esc_url( 'https://graphthemes.com/plush/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'View Premium Version', 'plush' ); ?></a>



</div>


<div>
	</div>