<?php


if( ! defined( 'PLUSH_WIDGET_PATH' ) ) {
    define( 'PLUSH_WIDGET_PATH', dirname( __FILE__ ) );
}


/**
* Author Profile Widget.
*/
require_once PLUSH_WIDGET_PATH . '/includes/class-graphthemes-widget-functions.php';

require_once PLUSH_WIDGET_PATH . '/includes/widgets/widget-author-profile.php';

require_once PLUSH_WIDGET_PATH . '/includes/widgets/widget-recent-posts.php';

require_once PLUSH_WIDGET_PATH . '/includes/widgets/widget-popular-posts.php';