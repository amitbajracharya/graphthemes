<?php

add_action( 'customize_register', 'plush_breadcrumbs' );
function plush_breadcrumbs( $wp_customize ) {

	$wp_customize->add_setting('plush_breadcrumbs_option', array(
        'sanitize_callback'     =>  'plush_sanitize_checkbox',
        'default'               =>  plush_get_default_breadcrumbs(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'plush_breadcrumbs_option', array(
        'label' => esc_html__('Enable Breadcrumbs', 'plush'),
        'section' => 'plush_general_customization_section',
        'settings' => 'plush_breadcrumbs_option',
        'type' => 'toggle',
    )));

}