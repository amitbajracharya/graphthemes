<?php

function graphthemes_get_social_links() {
    $social_links = array(
        esc_html__( 'Facebook', 'plush' ),
        esc_html__( 'Instagram', 'plush' ),
        esc_html__( 'Youtube', 'plush' ),
        esc_html__( 'LinkedIn', 'plush' ),
        esc_html__( 'Twitter', 'plush' ),
        esc_html__( 'Pinterest', 'plush' ),
        esc_html__( 'TikTok', 'plush' )
    );

    return $social_links;
}

add_action( 'customize_register', 'plush_social_links' );
function plush_social_links( $wp_customize ) {

    $social_links = graphthemes_get_social_links();

    $social_links_title = "<hr/><h2>".esc_html__( "Social Links:", 'plush' )."</h2>";

    $wp_customize->add_setting( 'social_links_title', array(
        'default' => '',
        'sanitize_callback' => 'wp_kses_post',
    ) );

    $wp_customize->add_control( new Plush_Custom_Text( $wp_customize ,'social_links_title',array(
        'section' => 'plush_general_customization_section',
        'label' => $social_links_title
    ) ) );


    if( $social_links ) {
        foreach( $social_links as $social_link ) {

            $wp_customize->add_setting( 'social_links_' . strtolower( $social_link ), array(
                'sanitize_callback' => 'esc_url_raw',
            ) );

            $wp_customize->add_control( 'social_links_' . strtolower( $social_link ), array(
                'label'       => $social_link,
                'section'     => 'plush_general_customization_section',
                'type'        => 'text'
            ) );

        }
    }

}