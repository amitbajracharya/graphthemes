<?php

add_action( 'customize_register', 'plush_sticky_menu' );
function plush_sticky_menu( $wp_customize ) {

	$wp_customize->add_setting('plush_sticky_menu_option', array(
        'sanitize_callback'     =>  'plush_sanitize_checkbox',
        'default'               =>  plush_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'plush_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'plush'),
        'section' => 'plush_general_customization_section',
        'settings' => 'plush_sticky_menu_option',
        'type' => 'toggle',
    )));

}