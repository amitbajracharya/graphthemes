<?php

/* Default General Customization */

function plush_get_default_breadcrumbs() {    
    return false;
}


function plush_get_default_sticky_menu() {    
    return false;
}


function plush_get_default_container_width() {    
    return 1400;
}