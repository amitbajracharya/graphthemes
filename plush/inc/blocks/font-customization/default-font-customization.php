<?php


/* Default Font Size */
function plush_get_default_font_size() {
    return 17;
}

function plush_get_default_logo_size() {
    return 20;
}


function plush_get_default_site_identity_font_size() {
    return 26;
}


function plush_get_default_font_weight() {
    return "300";
}

function plush_get_default_line_height() {
    return "1.4";
}