<?php

/* Default Font Family */
function plush_get_default_site_identity_font_family() {    
    return esc_html__( "Jost", 'plush' );
}

function plush_get_default_main_font_family() {    
    return esc_html__( "League Spartan", 'plush' );
}

function plush_get_default_secondary_font_family() {    
    return esc_html__( "Kameron", 'plush' );
}