<?php


/* Default Site Title Color */
function plush_get_default_site_title_color() {
    return "#000";
}

/* Default Primary Color */
function plush_get_default_primary_color() {
    return "#9f326c";
}

/* Default Secondary Color */
function plush_get_default_secondary_color() {
    return "#000000";
}

/* Default Light Color */
function plush_get_default_light_color() {
    return "#ffffff";
}

/* Default Grey Color */
function plush_get_default_grey_color() {
    return "#666666";
}

/* Default Dark Color */
function plush_get_default_dark_color() {
    return "#333333";
}

/* Default Background Color */
function plush_get_default_background_color() {
    return "fbf8f8";
}