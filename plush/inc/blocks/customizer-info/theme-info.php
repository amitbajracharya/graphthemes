<?php

add_action( 'customize_register', 'plush_customizer_theme_info' );

function plush_customizer_theme_info( $wp_customize ) {
	
    $wp_customize->add_section( 'plush_theme_info_section' , array(
		'title'       => esc_html__( '❂ Theme Info' , 'plush' ),
		'priority' => 2
	) );
    

	$wp_customize->add_setting( 'theme_info', array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
	) );
    
    $theme_info = '';
	
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Theme Details', 'plush' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/plush/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'plush' ) . '</a></span><hr>';

	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'How to use', 'plush' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/theme-docs/plush/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'plush' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'View Demo', 'plush' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/preview/?product_id=plush' ) . '" target="_blank">' . esc_html__( 'Click Here', 'plush' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Support Forum', 'plush' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://wordpress.org/support/theme/plush' ) . '" target="_blank">' . esc_html__( 'Click Here', 'plush' ) . '</a></span><hr>';

	$wp_customize->add_control( new Plush_Custom_Text( $wp_customize ,'theme_info',array(
		'section' => 'plush_theme_info_section',
		'label' => $theme_info
	) ) );

}