<?php

function plush_customizer_upgrade_to_pro( $wp_customize ) {

	$wp_customize->add_section( new Plush_Button_Control( $wp_customize, 'upgrade-to-pro',	array(
		'title'    => esc_html__( '★ Plush Pro ', 'plush' ),
		'type'	=> 'button',
		'pro_text' => esc_html__( 'Upgrade to Pro', 'plush' ),
		'pro_url'  => esc_url( 'https://graphthemes.com/plush/' ),
		'priority' => 1
	) )	);

	
}
add_action( 'customize_register', 'plush_customizer_upgrade_to_pro' );


function plush_enqueue_custom_admin_style() {
        wp_register_style( 'plush-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.css', false );
        wp_enqueue_style( 'plush-button' );

        wp_enqueue_script( 'plush-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.js', array( 'jquery' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'plush_enqueue_custom_admin_style' );