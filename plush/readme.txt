=== plush ===
Contributors: graphthemes
Tested up to: 6.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Plush, Copyright 2022 Graphthemes
Plush is distributed under the terms of the GNU GPL



== Installation ==
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.




== Changelog ==

= 1.0.2 - January 13, 2023 =
* screenshot image compressed for better theme performance.

= 1.0.1 - January 13, 2023 =
* Fixed gravatar url in author widget.

= 1.0.0 - January 06, 2023 =
* Initial version release

== Credits ==

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/971394

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/941609


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1064850

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1410042

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/879276


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1343040


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1170415


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/952741








	Images/Icons Used in customizer
	All the images/icons used in the customizer are created by our own.
	License: GPL v2 or later

	Masonry
	Source: https://masonry.desandro.com/
	License: [MIT](https://desandro.mit-license.org/)


	Underscores:
	Author: 2012-2015 Automattic
	Source: http://underscores.me
	License: GPL v2 or later (https://www.gnu.org/licenses/gpl-2.0.html)


	Google Fonts
	Source: https://fonts.google.com/
	License: Apache License, version 2.0

