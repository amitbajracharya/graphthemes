jQuery( function( $ ) {

	wp.customize( 'logo_size', function( value ) {
		value.bind( function( to ) {
			if ( to < 10 ) {
				document.body.style.setProperty( '--logo-size', 10 + 'px' );
			} else if( to > 225 ) {
				document.body.style.setProperty( '--logo-size', 225 + 'px' );
			} else {
				document.body.style.setProperty( '--logo-size', to + 'px' );
			}
		} );
	} );

} );
