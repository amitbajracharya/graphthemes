<?php

add_action( 'customize_register', 'edward_blog_logo_size' );
function edward_blog_logo_size( $wp_customize ) {

    $wp_customize->add_setting( 'logo_size', array(
        'default'     => edward_blog_get_default_logo_size(),
        'transport'   => 'postMessage',
        'sanitize_callback' => 'edward_blog_sanitize_logo_size'
    ) );

    $wp_customize->add_control( 'logo_size', array(
        'type'        => 'number',
        'settings'    => 'logo_size',
        'priority'  => 8,
        'label'       =>  esc_html__( 'Logo Size', 'edward-blog' ),
        'description' => esc_html__( 'Logo size accepts values between 10 to 225.', 'edward-blog' ),
        'section'     => 'title_tagline',
        'input_attrs' => array(
            'min' => 10,
            'max' => 225,
        )
    ) );

}

add_action( 'customize_preview_init', 'edward_blog_logo_size_enqueue_scripts' );
function edward_blog_logo_size_enqueue_scripts() {
    wp_enqueue_script( 'graphthemes-logo-size-customizer', get_template_directory_uri() . '/inc/blocks/font-customization/logo-size/customizer-logo-size.js', array('jquery'), '', true );
}


add_action( 'wp_enqueue_scripts', 'edward_blog_logo_size_dynamic_css' );
function edward_blog_logo_size_dynamic_css() {

    $logo_size = esc_attr( get_theme_mod( 'logo_size', edward_blog_get_default_logo_size() ) );
    if ( $logo_size < 10 ) {
        $logo_size = 10;
    } else if( $logo_size > 225 ) {
        $logo_size = 225;
    }
    $logo_size .= 'px';


    $dynamic_css = ":root { --logo-size: $logo_size; }";

    wp_add_inline_style( 'edward-blog', $dynamic_css );
}