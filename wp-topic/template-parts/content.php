<?php
/**
 * Template part for displaying posts.
 *
 * @package pagoda-press
 */
?>


<?php
    $show_hide_image = get_theme_mod( 'post_snippet_hide_show_featured_image', pagoda_press_get_default_post_snippet_featured_image() );
    $show_hide_date = get_theme_mod( 'post_snippet_hide_show_date', pagoda_press_get_default_post_snippet_date() );
    $show_hide_author = get_theme_mod( 'post_snippet_hide_show_author', pagoda_press_get_default_post_snippet_author() );
    $show_hide_comment = get_theme_mod( 'post_snippet_hide_show_comment', pagoda_press_get_default_post_snippet_comment() );
    $show_hide_categories = get_theme_mod( 'post_snippet_hide_show_category', pagoda_press_get_default_post_snippet_category() );
    $show_hide_tags = get_theme_mod( 'post_snippet_hide_show_tag', pagoda_press_get_default_post_snippet_tag() );
    $show_hide_social_share = get_theme_mod( 'post_snippet_hide_show_social_share', pagoda_press_get_default_post_snippet_social_share() );
    $social_share = get_theme_mod( 'post_snippet_social_share_options', pagoda_press_get_default_post_snippet_social_share_options() );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="news-snippet">
    


        <?php if ( $show_hide_image && has_post_thumbnail() ) : ?>
            <?php $thumbnail_size = get_theme_mod( 'post_snippet_featured_image_size', pagoda_press_get_default_post_snippet_featured_image_size() ); ?>
            <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="featured-image">
                <?php the_post_thumbnail( $thumbnail_size ); ?>
            </a>
        <?php endif; ?>
        <div class="summary">


            


                <h3 class="news-title"><a href="<?php echo esc_url( get_permalink() ); ?>"
                rel="bookmark"><?php the_title(); ?></a></h3>

                <?php 
            if( $show_hide_author ) { ?>
                    <div class="post-author">
                        by: <a class="url fn n"
                        href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
                        <?php $avatar = get_avatar( get_the_author_meta( 'ID' ), $size = 60 ); ?>
                        <?php echo esc_html( get_the_author() ); ?>
                    </a>
                </div>
            <?php } ?>

                

                <div class="excerpt">
                    <?php $excerpt_length = get_theme_mod( 'post_snippet_excerpt_size', pagoda_press_get_default_post_snippet_excerpt_size() ); ?>

                    <?php echo wp_trim_words( get_the_excerpt(), $excerpt_length ); ?>
                </div>






            <?php $readmore_show_hide = get_theme_mod( 'post_snippet_hide_show_readmore', pagoda_press_get_default_post_snippet_show_hide_read_more() ); ?>
            <?php $readmore_text = get_theme_mod( 'post_snippet_readmore_text', pagoda_press_get_default_post_snippet_read_more_text() ); ?>

            
                <div class="ifoot info">

                    <div class="readmore"><a href="#" rel="bookmark" class="btn">Download Now</a> <a href="#" rel="bookmark">View Demo</a></div>

                    <?php if( $show_hide_social_share && $social_share ) { ?>
                        <div class="social-share">
                            <?php get_template_part( 'inc/blocks/includes/template', 'social-share', $social_share ); ?>                    
                        </div>
                    <?php } ?>

                </div>

			



                </div>
            </div>
        </div>