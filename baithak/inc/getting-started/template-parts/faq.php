<div class="das-faqs">
<h1><?php esc_html_e("FAQ's", 'baithak'); ?></h1>
<div class="das-wrap">
	<h3><?php esc_html_e("1. What is the difference between Free and Pro?", 'baithak'); ?></h3>
	<p><?php esc_html_e("Both Free and Premium version of the themes are well-built. If you are looking for more color options, font options, whitelabeling and more, then you can upgrade to premium.", 'baithak'); ?></p>
	<p><?php esc_html_e("The premium version lets you have better control over the theme as it comes with more customization options.", 'baithak'); ?> <a href="<?php echo esc_url( 'https://graphthemes.com/baithak/' ); ?>" target="_blank"  class="link"><?php esc_html_e("here", 'baithak'); ?></a>.</p>
	<hr>
	<h3><?php esc_html_e("2. What are the advantages of upgrading to the Premium version?", 'baithak'); ?></h3>
	<p><?php esc_html_e("With Premium version, besides the extra features and frequent updates, you get premium support. If you run into any theme issues, you will get a lot quicker response compared to the free support.", 'baithak'); ?></p>
	<hr>
	<h3><?php esc_html_e("3. How do I change the copyright text?", 'baithak'); ?></h3>
	<p><?php esc_html_e("You can change the copyright text going to", 'baithak'); ?> <a href="<?php echo esc_url (admin_url( '/customize.php' ));?>" class="link"><?php esc_html_e("Appearance > Customize > Footer Copyright", 'baithak'); ?></a>. <?php esc_html_e("Upgrade to the", 'baithak'); ?> <a href="<?php echo esc_url( 'https://graphthemes.com/baithak/' ); ?>" target="_blank"  class="link"><?php esc_html_e("Premium version", 'baithak'); ?></a> <?php esc_html_e("to unlock this feature.", 'baithak'); ?></p>
	<hr>

	<h2><?php esc_html_e("For more help, read our documentation", 'baithak'); ?></h2>
	<a href="<?php echo esc_url( 'https://graphthemes.com/docs/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e("View Documentation", 'baithak'); ?></a>


	<div class="baithak-panel">
		<div class="baithak-panel-content">
			<div class="theme-title">
				<h4><?php esc_html_e( 'If you like our theme, please leave a review or Contact us for technical support.', 'baithak' ); ?></h4>
			</div>
			<a href="<?php echo esc_url( 'https://wordpress.org/support/theme/baithak/reviews/#new-post' ); ?>" target="_blank" class="btn btn-primary"><?php esc_html_e( 'Rate this theme', 'baithak' ); ?></a><a href="<?php echo esc_url( 'https://graphthemes.com/support/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'Contact Us', 'baithak' ); ?></a> 
		</div>
	</div>

</div>
</div>