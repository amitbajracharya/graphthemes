<div class="get-started-info">
	<h1><?php esc_html_e("Free v/s Pro", 'baithak'); ?></h1>
	<table class="widefat">
		<thead><tr><th width="75%"></th><th><?php esc_html_e("Free", 'baithak'); ?></th><th><?php esc_html_e("Pro", 'baithak'); ?></th></tr></thead>
		<tbody>
			<tr><td><?php esc_html_e("1000+ Google Fonts", 'baithak'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Unlimited Color Option", 'baithak'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Footer Copyright", 'baithak'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("24/7 Support", 'baithak'); ?></td><td><span class="dashicons dashicons-no"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site Logo Management", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Site width controller", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("In-built Social Sharing", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Pagination Options", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Child Theme Compatible", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Floating/Sticky Menu", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Header Background Image", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Blog Options", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Show/Hide Author Info", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Scroll to Top button", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Responsive Layout", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Live Customizer", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Theme Options", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Breadcrumb", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("SEO Optimized", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>
			<tr><td><?php esc_html_e("Lightweight", 'baithak'); ?></td><td><span class="dashicons dashicons-yes"></span></td><td><span class="dashicons dashicons-yes"></span></td></tr>			
		<tbody>
		<tfoot>
			<tr><td></td><td colspan="2"><a style="float:right;" href="<?php echo esc_url( 'https://graphthemes.com/baithak/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'Upgrade to Premium', 'baithak' ); ?></a></td></tr>
		</tfoot>
	</table>
	
</div>
