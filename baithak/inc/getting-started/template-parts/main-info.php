<div class="main-info-detail">

	<h1><?php esc_html_e( 'Baithak', 'baithak' ); ?></h1>

	<p><?php esc_html_e( 'An easy to use WordPress theme for Passionate Bloggers, Writers, Editors and more.  This theme is built with hassle-free and powerful customization options that allow users to easily customize without writing a single line of code. You can try customizing theme now without any risk.', 'baithak' ); ?></p>
	
	<a class="btn btn-success" href="<?php echo esc_url( admin_url( '/customize.php' ) ); ?>">
		<?php esc_html_e( '⨳ Start Theme Customization', 'baithak' ); ?>
	</a>


	<?php esc_html_e("Need help?", 'baithak'); ?> <a href="<?php echo esc_url('https://graphthemes.com/support/'); ?>" target="_blank" class="link"><?php esc_html_e("Contact Us", 'baithak'); ?></a> <?php esc_html_e("or email us at: hello@graphthemes.com", 'baithak'); ?>

	<hr>
	<h2><?php esc_html_e( "Upgrade to Pro", 'baithak' ); ?></h2>
	<h4><?php esc_html_e("Need more color options, font options, edit footer copyright info?", 'baithak'); ?></h4>
	<a href="<?php echo esc_url( 'https://graphthemes.com/baithak/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'View Premium Version', 'baithak' ); ?></a>



</div>


<div>
	</div>