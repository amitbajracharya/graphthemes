<?php


/* Default Font Size */
function baithak_get_default_font_size() {
    return 16;
}

function baithak_get_default_logo_size() {
    return 20;
}


function baithak_get_default_site_identity_font_size() {
    return 26;
}


function baithak_get_default_font_weight() {
    return "400";
}

function baithak_get_default_line_height() {
    return "1.7";
}