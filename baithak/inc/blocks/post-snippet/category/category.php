<?php

add_action( 'customize_register', 'baithak_post_snippet_category' );
function baithak_post_snippet_category( $wp_customize ) {

	$wp_customize->add_setting( 'post_snippet_hide_show_category', array(
        'sanitize_callback'     =>  'baithak_sanitize_checkbox',
        'default'               =>  baithak_get_default_post_snippet_category()
    ) );

    $wp_customize->add_control( new Graphthemes_Toggle_Control( $wp_customize, 'post_snippet_hide_show_category', array(
        'label' => esc_html__( 'Show/Hide Categories','baithak' ),
        'section' => 'baithak_post_snippet_customization_section',
        'settings' => 'post_snippet_hide_show_category',
        'type'=> 'toggle',
    ) ) );

}