<?php

function baithak_customizer_upgrade_to_pro( $wp_customize ) {

	$wp_customize->add_section( new Baithak_Button_Control( $wp_customize, 'upgrade-to-pro',	array(
		'title'    => esc_html__( '★ Baithak Pro ', 'baithak' ),
		'type'	=> 'button',
		'pro_text' => esc_html__( 'Upgrade to Pro', 'baithak' ),
		'pro_url'  => esc_url( 'https://graphthemes.com/baithak/' ),
		'priority' => 1
	) )	);

	
}
add_action( 'customize_register', 'baithak_customizer_upgrade_to_pro' );


function baithak_enqueue_custom_admin_style() {
        wp_register_style( 'baithak-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.css', false );
        wp_enqueue_style( 'baithak-button' );

        wp_enqueue_script( 'baithak-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.js', array( 'jquery' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'baithak_enqueue_custom_admin_style' );