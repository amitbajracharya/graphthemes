<?php

/* Default Font Family */
function baithak_get_default_site_identity_font_family() {    
    return esc_html__( "Jost", 'baithak' );
}

function baithak_get_default_main_font_family() {    
    return esc_html__( "David Libre", 'baithak' );
}

function baithak_get_default_secondary_font_family() {    
    return esc_html__( "Jost", 'baithak' );
}