<?php

/* Default General Customization */

function baithak_get_default_breadcrumbs() {    
    return false;
}


function baithak_get_default_sticky_menu() {    
    return false;
}


function baithak_get_default_container_width() {    
    return 1400;
}