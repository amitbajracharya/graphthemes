<?php

add_action( 'customize_register', 'baithak_sticky_menu' );
function baithak_sticky_menu( $wp_customize ) {

	$wp_customize->add_setting('baithak_sticky_menu_option', array(
        'sanitize_callback'     =>  'baithak_sanitize_checkbox',
        'default'               =>  baithak_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'baithak_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'baithak'),
        'section' => 'baithak_general_customization_section',
        'settings' => 'baithak_sticky_menu_option',
        'type' => 'toggle',
    )));

}