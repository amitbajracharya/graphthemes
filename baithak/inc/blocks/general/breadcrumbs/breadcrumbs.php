<?php

add_action( 'customize_register', 'baithak_breadcrumbs' );
function baithak_breadcrumbs( $wp_customize ) {

	$wp_customize->add_setting('baithak_breadcrumbs_option', array(
        'sanitize_callback'     =>  'baithak_sanitize_checkbox',
        'default'               =>  baithak_get_default_breadcrumbs(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'baithak_breadcrumbs_option', array(
        'label' => esc_html__('Enable Breadcrumbs', 'baithak'),
        'section' => 'baithak_general_customization_section',
        'settings' => 'baithak_breadcrumbs_option',
        'type' => 'toggle',
    )));

}