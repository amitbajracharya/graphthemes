<?php


/* Default Site Title Color */
function baithak_get_default_site_title_color() {
    return "#000";
}

/* Default Primary Color */
function baithak_get_default_primary_color() {
    return "#20c0e3";
}

/* Default Secondary Color */
function baithak_get_default_secondary_color() {
    return "#000000";
}

/* Default Light Color */
function baithak_get_default_light_color() {
    return "#ffffff";
}

/* Default Grey Color */
function baithak_get_default_grey_color() {
    return "#969aa5";
}

/* Default Dark Color */
function baithak_get_default_dark_color() {
    return "#404040";
}

/* Default Background Color */
function baithak_get_default_background_color() {
    return "f5f7f9";
}