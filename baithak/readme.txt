=== baithak ===
Contributors: graphthemes
Tested up to: 6.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Baithak, Copyright 2022 Graphthemes
Baithak is distributed under the terms of the GNU GPL



== Installation ==
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.




== Changelog ==

= 1.0.2 - September 5, 2022 =
* Added numbered pagination.

= 1.0.1 - August 17, 2022 =
* .pot file updated.

= 1.0.0 - August 12, 2022 =
* Initial version release

== Credits ==

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1663148

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1663146


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1662524

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1662152

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1662320


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1660100


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1659482


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1658268


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1656676


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1653902


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1653572


	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1653205






	Images/Icons Used in customizer
	All the images/icons used in the customizer are created by our own.
	License: GPL v2 or later

	Masonry
	Source: https://masonry.desandro.com/
	License: [MIT](https://desandro.mit-license.org/)


	Underscores:
	Author: 2012-2015 Automattic
	Source: http://underscores.me
	License: GPL v2 or later (https://www.gnu.org/licenses/gpl-2.0.html)


	Google Fonts
	Source: https://fonts.google.com/
	License: Apache License, version 2.0

