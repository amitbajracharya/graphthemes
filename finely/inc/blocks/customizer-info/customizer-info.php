<?php

if (!fin_fs()->is__premium_only()) {
	require dirname(__FILE__) . '/upgrade-to-pro.php';
}

require dirname(__FILE__) . '/theme-info.php';

if (!fin_fs()->is__premium_only()) {
	require dirname(__FILE__) . '/premium-features.php';
}
