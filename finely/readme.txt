=== city girl ===
Contributors: graphthemes
Tested up to: 6.0
License: GNU General Public License v3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Finley, Copyright 2022 Graphthemes
Finley is distributed under the terms of the GNU GPL



== Installation ==
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.




== Changelog ==

= 1.0.0 - March 19, 2023 =
	* Initial release

== Credits ==

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/113574

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/727881

	Image for theme screenshot, Credit pxhere.com
	License: CC0 1.0 Universal (CC0 1.0)
	License URL: https://creativecommons.org/publicdomain/zero/1.0/
	Source: https://pxhere.com/en/photo/1324058



	Images/Icons Used in customizer
	All the images/icons used in the customizer are created by our own.
	License: GPL v2 or later


	Underscores:
	Author: 2012-2015 Automattic
	Source: http://underscores.me
	License: GPL v2 or later (https://www.gnu.org/licenses/gpl-2.0.html)


	Google Fonts
	Source: https://fonts.google.com/
	License: Apache License, version 2.0