jQuery( function( $ ) {

	wp.customize('site_identity_font_size',function ( value ) {
		value.bind( function( to ) {
			if ( to < 10 ) {
				document.body.style.setProperty( '--site-identity-font-size', 10 + 'px' );
			} else if( to > 225 ) {
				document.body.style.setProperty( '--site-identity-font-size', 100 + 'px' );
			} else {
				document.body.style.setProperty( '--site-identity-font-size', to + 'px' );
			}
		} );
	} );

} );