<div class="main-info-detail">

	<h1><?php esc_html_e( 'Follow', 'follow' ); ?></h1>

	<p><?php esc_html_e( 'An easy to use WordPress theme for Passionate Bloggers, Writers, Editors and more.  This theme is built with hassle-free and powerful customization options that allow users to easily customize without writing a single line of code. You can try customizing theme now without any risk.', 'follow' ); ?></p>
	
	<a class="btn btn-success" href="<?php echo esc_url( admin_url( '/customize.php' ) ); ?>">
		<?php esc_html_e( '⨳ Start Theme Customization', 'follow' ); ?>
	</a>


	<?php esc_html_e("Need help?", 'follow'); ?> <a href="<?php echo esc_url('https://graphthemes.com/support/'); ?>" target="_blank" class="link"><?php esc_html_e("Contact Us", 'follow'); ?></a> <?php esc_html_e("or email us at: hello@graphthemes.com", 'follow'); ?>

	<hr>
	<h2><?php esc_html_e( "Upgrade to Pro", 'follow' ); ?></h2>
	<h4><?php esc_html_e("Need more color options, font options, edit footer copyright info?", 'follow'); ?></h4>
	<a href="<?php echo esc_url( 'https://graphthemes.com/follow/' ); ?>" target="_blank" class="btn btn-success"><?php esc_html_e( 'View Premium Version', 'follow' ); ?></a>



</div>


<div>
	</div>