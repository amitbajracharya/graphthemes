<?php


/* Default Site Title Color */
function follow_get_default_site_title_color() {
    return "#000";
}

/* Default Primary Color */
function follow_get_default_primary_color() {
    return "#66c757";
}

/* Default Secondary Color */
function follow_get_default_secondary_color() {
    return "#000000";
}

/* Default Light Color */
function follow_get_default_light_color() {
    return "#ffffff";
}

/* Default Grey Color */
function follow_get_default_grey_color() {
    return "#969aa5";
}

/* Default Dark Color */
function follow_get_default_dark_color() {
    return "#333333";
}

/* Default Background Color */
function follow_get_default_background_color() {
    return "ffffff";
}