<?php

add_action( 'customize_register', 'follow_post_snippet_author' );
function follow_post_snippet_author( $wp_customize ) {

	$wp_customize->add_setting( 'post_snippet_hide_show_author', array(
        'sanitize_callback'     =>  'follow_sanitize_checkbox',
        'default'               =>  follow_get_default_post_snippet_author()
    ) );

    $wp_customize->add_control( new Graphthemes_Toggle_Control( $wp_customize, 'post_snippet_hide_show_author', array(
        'label' => esc_html__( 'Show/Hide Author','follow' ),
        'section' => 'follow_post_snippet_customization_section',
        'settings' => 'post_snippet_hide_show_author',
        'type'=> 'toggle',
    ) ) );

}