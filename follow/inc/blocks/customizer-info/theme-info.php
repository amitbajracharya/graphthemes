<?php

add_action( 'customize_register', 'follow_customizer_theme_info' );

function follow_customizer_theme_info( $wp_customize ) {
	
    $wp_customize->add_section( 'follow_theme_info_section' , array(
		'title'       => esc_html__( '❂ Theme Info' , 'follow' ),
		'priority' => 2
	) );
    

	$wp_customize->add_setting( 'theme_info', array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
	) );
    
    $theme_info = '';
	
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Theme Details', 'follow' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/follow/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'follow' ) . '</a></span><hr>';

	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'How to use', 'follow' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/theme-docs/follow/' ) . '" target="_blank">' . esc_html__( 'Click Here', 'follow' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'View Demo', 'follow' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://graphthemes.com/preview/?product_id=follow' ) . '" target="_blank">' . esc_html__( 'Click Here', 'follow' ) . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__( 'Support Forum', 'follow' ) . ': </label><a class="button alignright" href="' . esc_url( 'https://wordpress.org/support/theme/follow' ) . '" target="_blank">' . esc_html__( 'Click Here', 'follow' ) . '</a></span><hr>';

	$wp_customize->add_control( new Follow_Custom_Text( $wp_customize ,'theme_info',array(
		'section' => 'follow_theme_info_section',
		'label' => $theme_info
	) ) );

}