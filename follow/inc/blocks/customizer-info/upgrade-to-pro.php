<?php

function follow_customizer_upgrade_to_pro( $wp_customize ) {

	$wp_customize->add_section( new Follow_Button_Control( $wp_customize, 'upgrade-to-pro',	array(
		'title'    => esc_html__( '★ Follow Pro ', 'follow' ),
		'type'	=> 'button',
		'pro_text' => esc_html__( 'Upgrade to Pro', 'follow' ),
		'pro_url'  => esc_url( 'https://graphthemes.com/follow/' ),
		'priority' => 1
	) )	);

	
}
add_action( 'customize_register', 'follow_customizer_upgrade_to_pro' );


function follow_enqueue_custom_admin_style() {
        wp_register_style( 'follow-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.css', false );
        wp_enqueue_style( 'follow-button' );

        wp_enqueue_script( 'follow-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.js', array( 'jquery' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'follow_enqueue_custom_admin_style' );