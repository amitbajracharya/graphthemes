<?php

/* Default Font Family */
function follow_get_default_site_identity_font_family() {    
    return esc_html__( "Playfair Display", 'follow' );
}

function follow_get_default_main_font_family() {    
    return esc_html__( "Jost", 'follow' );
}

function follow_get_default_secondary_font_family() {    
    return esc_html__( "Playfair Display", 'follow' );
}