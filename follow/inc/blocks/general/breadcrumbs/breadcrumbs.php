<?php

add_action( 'customize_register', 'follow_breadcrumbs' );
function follow_breadcrumbs( $wp_customize ) {

	$wp_customize->add_setting('follow_breadcrumbs_option', array(
        'sanitize_callback'     =>  'follow_sanitize_checkbox',
        'default'               =>  follow_get_default_breadcrumbs(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'follow_breadcrumbs_option', array(
        'label' => esc_html__('Enable Breadcrumbs', 'follow'),
        'section' => 'follow_general_customization_section',
        'settings' => 'follow_breadcrumbs_option',
        'type' => 'toggle',
    )));

}