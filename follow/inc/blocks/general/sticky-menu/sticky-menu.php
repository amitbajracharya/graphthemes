<?php

add_action( 'customize_register', 'follow_sticky_menu' );
function follow_sticky_menu( $wp_customize ) {

	$wp_customize->add_setting('follow_sticky_menu_option', array(
        'sanitize_callback'     =>  'follow_sanitize_checkbox',
        'default'               =>  follow_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'follow_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'follow'),
        'section' => 'follow_general_customization_section',
        'settings' => 'follow_sticky_menu_option',
        'type' => 'toggle',
    )));

}