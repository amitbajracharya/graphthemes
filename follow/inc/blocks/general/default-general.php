<?php

/* Default General Customization */

function follow_get_default_breadcrumbs() {    
    return false;
}


function follow_get_default_sticky_menu() {    
    return false;
}


function follow_get_default_container_width() {    
    return 1400;
}