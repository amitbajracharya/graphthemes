<?php


/* Default Font Size */
function follow_get_default_font_size() {
    return 16;
}

function follow_get_default_logo_size() {
    return 60;
}


function follow_get_default_site_identity_font_size() {
    return 40;
}


function follow_get_default_font_weight() {
    return "400";
}

function follow_get_default_line_height() {
    return "1.6";
}