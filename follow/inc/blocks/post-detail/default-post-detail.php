<?php

/* Default Post Detail Author*/
function follow_get_default_post_detail_author() {
    return true;
}

/* Default Post Detail Category*/
function follow_get_default_post_detail_category() {
    return true;
}

/* Default Post Detail Comment*/
function follow_get_default_post_detail_comment() {
    return true;
}

/* Default Post Detail Date*/
function follow_get_default_post_detail_date() {
    return true;
}

/* Default Post Detail Featured Image*/
function follow_get_default_post_detail_featured_image() {
    return true;
}

/* Default Post Detail Tag*/
function follow_get_default_post_detail_featured_image_size() {
    return esc_html__( 'large', 'follow' );
}

/* Default Post Detail Tag*/
function follow_get_default_post_detail_tag() {
    return true;
}

/* Default Post Detail Social Share */
function follow_get_default_post_detail_social_share() {
    return true;
}

/* Default Post Detail Social Share Options*/
function follow_get_default_post_detail_social_share_options() {
    return array();
}

/* Default Post Detail Author Block Options */
function follow_get_default_post_detail_author_block() {
    return true;
}

/* Default Post Detail Related Articles Options*/
function follow_get_default_post_detail_related_articles() {
    return true;
}

/* Default Post Detail Related Articles Title */
function follow_get_default_post_detail_related_articles_title() {
    return esc_html__( "Related Articles", 'follow' );
}