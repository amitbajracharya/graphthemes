<div class="get-started-info">
	<h1><?php esc_html_e("Free v/s Pro", 'city-girl'); ?></h1>
	<table class="widefat">
		<thead>
			<tr>
				<th width="75%"></th>
				<th><?php esc_html_e("Free", 'city-girl'); ?></th>
				<th><?php esc_html_e("Pro", 'city-girl'); ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?php esc_html_e("1000+ Google Fonts", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-no"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Unlimited Color Option", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-no"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Footer Copyright", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-no"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("24/7 Support", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-no"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Site Logo Management", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Site width controller", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("In-built Social Sharing", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Pagination Options", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Child Theme Compatible", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Floating/Sticky Menu", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Header Background Image", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Blog Options", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Show/Hide Author Info", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Scroll to Top button", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Responsive Layout", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Live Customizer", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Theme Options", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Breadcrumb", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("SEO Optimized", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
			<tr>
				<td><?php esc_html_e("Lightweight", 'city-girl'); ?></td>
				<td><span class="dashicons dashicons-yes"></span></td>
				<td><span class="dashicons dashicons-yes"></span></td>
			</tr>
		<tbody>
		<tfoot>
			<tr>
				<td></td>
				<td colspan="2"><a style="float:right;" href="<?php echo esc_url('https://graphthemes.com/city-girl/'); ?>" target="_blank" class="btn btn-success"><?php esc_html_e('Upgrade to Premium', 'city-girl'); ?></a></td>
			</tr>
		</tfoot>
	</table>

</div>