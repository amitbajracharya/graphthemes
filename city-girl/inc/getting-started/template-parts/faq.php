<div class="das-faqs">
	<h1><?php esc_html_e("FAQ's", 'city-girl'); ?></h1>
	<div class="das-wrap">
		<h3><?php esc_html_e("1. What is the difference between Free and Pro?", 'city-girl'); ?></h3>
		<p><?php esc_html_e("Both Free and Premium version of the themes are well-built. If you are looking for more color options, font options, whitelabeling and more, then you can upgrade to premium.", 'city-girl'); ?></p>
		<p><?php esc_html_e("The premium version lets you have better control over the theme as it comes with more customization options.", 'city-girl'); ?> <a href="<?php echo esc_url('https://graphthemes.com/city-girl/'); ?>" target="_blank" class="link"><?php esc_html_e("here", 'city-girl'); ?></a>.</p>
		<hr>
		<h3><?php esc_html_e("2. What are the advantages of upgrading to the Premium version?", 'city-girl'); ?></h3>
		<p><?php esc_html_e("With Premium version, besides the extra features and frequent updates, you get premium support. If you run into any theme issues, you will get a lot quicker response compared to the free support.", 'city-girl'); ?></p>
		<hr>
		<h3><?php esc_html_e("3. How do I change the copyright text?", 'city-girl'); ?></h3>
		<p><?php esc_html_e("You can change the copyright text going to", 'city-girl'); ?> <a href="<?php echo esc_url(admin_url('/customize.php')); ?>" class="link"><?php esc_html_e("Appearance > Customize > Footer Copyright", 'city-girl'); ?></a>. <?php esc_html_e("Upgrade to the", 'city-girl'); ?> <a href="<?php echo esc_url('https://graphthemes.com/city-girl/'); ?>" target="_blank" class="link"><?php esc_html_e("Premium version", 'city-girl'); ?></a> <?php esc_html_e("to unlock this feature.", 'city-girl'); ?></p>
		<hr>

		<h2><?php esc_html_e("For more help, read our documentation", 'city-girl'); ?></h2>
		<a href="<?php echo esc_url('https://graphthemes.com/docs/'); ?>" target="_blank" class="btn btn-success"><?php esc_html_e("View Documentation", 'city-girl'); ?></a>


		<div class="city-girl-panel">
			<div class="city-girl-panel-content">
				<div class="theme-title">
					<h4><?php esc_html_e('If you like our theme, please leave a review or Contact us for technical support.', 'city-girl'); ?></h4>
				</div>
				<a href="<?php echo esc_url('https://wordpress.org/support/theme/city-girl/reviews/#new-post'); ?>" target="_blank" class="btn btn-primary"><?php esc_html_e('Rate this theme', 'city-girl'); ?></a><a href="<?php echo esc_url('https://graphthemes.com/support/'); ?>" target="_blank" class="btn btn-success"><?php esc_html_e('Contact Us', 'city-girl'); ?></a>
			</div>
		</div>

	</div>
</div>