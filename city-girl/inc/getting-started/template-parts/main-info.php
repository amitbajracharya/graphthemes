<div class="main-info-detail">

	<h1><?php esc_html_e('City Girl', 'city-girl'); ?></h1>

	<p><?php esc_html_e("Female Travelers Blog Theme. Citygirl is a sleek and stylish WordPress theme designed specifically for female travelers looking to build a blog. With its mobile-friendly design, fast loading speeds, and easy customization options, Citygirl is the perfect choice for any woman on the go. The theme features a variety of color and font options, allowing you to create a unique and personalized look for your blog. Whether you're documenting your travels, sharing your experiences, or offering travel tips and advice, Citygirl has everything you need to create a beautiful and professional-looking blog. So, it's a perfect fit for those who want to share their travel stories with others.", 'city-girl'); ?></p>

	<a class="btn btn-success" href="<?php echo esc_url(admin_url('/customize.php')); ?>">
		<?php esc_html_e('⨳ Start Theme Customization', 'city-girl'); ?>
	</a>


	<?php esc_html_e("Need help?", 'city-girl'); ?> <a href="<?php echo esc_url('https://graphthemes.com/support/'); ?>" target="_blank" class="link"><?php esc_html_e("Contact Us", 'city-girl'); ?></a> <?php esc_html_e("or email us at: hello@graphthemes.com", 'city-girl'); ?>

	<hr>
	<h2><?php esc_html_e("Upgrade to Pro", 'city-girl'); ?></h2>
	<h4><?php esc_html_e("Need more color options, font options, edit footer copyright info?", 'city-girl'); ?></h4>
	<a href="<?php echo esc_url('https://graphthemes.com/city-girl/'); ?>" target="_blank" class="btn btn-success"><?php esc_html_e('View Premium Version', 'city-girl'); ?></a>



</div>


<div>
</div>