<?php


/* Default Font Size */
function city_girl_get_default_font_size()
{
    return 15;
}

function city_girl_get_default_logo_size()
{
    return 20;
}


function city_girl_get_default_site_identity_font_size()
{
    return 30;
}


function city_girl_get_default_font_weight()
{
    return "400";
}

function city_girl_get_default_line_height()
{
    return "1.7";
}
