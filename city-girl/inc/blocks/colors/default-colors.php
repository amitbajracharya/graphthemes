<?php


/* Default Site Title Color */
function city_girl_get_default_site_title_color()
{
    return "#69bc93";
}

/* Default Primary Color */
function city_girl_get_default_primary_color()
{
    return "#69bc93";
}

/* Default Secondary Color */
function city_girl_get_default_secondary_color()
{
    return "#69bc93";
}

/* Default Light Color */
function city_girl_get_default_light_color()
{
    return "#ffffff";
}

/* Default Grey Color */
function city_girl_get_default_grey_color()
{
    return "#969aa5";
}

/* Default Dark Color */
function city_girl_get_default_dark_color()
{
    return "#444444";
}

/* Default Background Color */
function city_girl_get_default_background_color()
{
    return "f6fbf6";
}
