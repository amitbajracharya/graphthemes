<?php

/* Default Font Family */
function city_girl_get_default_site_identity_font_family()
{
    return esc_html__("Cormorant", 'city-girl');
}

function city_girl_get_default_main_font_family()
{
    return esc_html__("Proza Libre", 'city-girl');
}

function city_girl_get_default_secondary_font_family()
{
    return esc_html__("Cormorant", 'city-girl');
}
