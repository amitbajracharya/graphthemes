<?php

add_action('customize_register', 'city_girl_post_snippet_social_share');
function city_girl_post_snippet_social_share($wp_customize)
{

    $wp_customize->add_setting('post_snippet_hide_show_social_share', array(
        'sanitize_callback'     =>  'city_girl_sanitize_checkbox',
        'default'               =>  city_girl_get_default_post_snippet_social_share()
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'post_snippet_hide_show_social_share', array(
        'label' => esc_html__('Enable Social Share', 'city-girl'),
        'section' => 'city_girl_post_snippet_customization_section',
        'settings' => 'post_snippet_hide_show_social_share',
        'type' => 'toggle',
    )));
}




add_action('customize_register', 'city_girl_post_snippet_social_share_options');
function city_girl_post_snippet_social_share_options($wp_customize)
{

    $wp_customize->add_setting('post_snippet_social_share_options', array(
        'sanitize_callback' => 'city_girl_sanitize_array',
        'default'     => city_girl_get_default_post_snippet_social_share_options()
    ));

    $wp_customize->add_control(new Graphthemes_Multi_Check_Control($wp_customize, 'post_snippet_social_share_options', array(
        'label' => esc_html__('Social Shares', 'city-girl'),
        'section' => 'city_girl_post_snippet_customization_section',
        'settings' => 'post_snippet_social_share_options',
        'type' => 'multi-check',
        'choices'     => array(
            'facebook' => esc_html__('Facebook', 'city-girl'),
            'twitter' => esc_html__('Twitter', 'city-girl'),
            'pinterest' => esc_html__('Pinterest', 'city-girl'),
            'linkedin' => esc_html__('LinkedIn', 'city-girl'),
            'email' => esc_html__('Email', 'city-girl'),
        ),
        'active_callback' => function () {
            return get_theme_mod('post_snippet_hide_show_social_share', city_girl_get_default_post_snippet_social_share());
        }
    )));

    $wp_customize->add_setting('twitter_id', array(
        'sanitize_callback' =>  'wp_kses_post',
    ));

    $wp_customize->add_control('twitter_id', array(
        'label' =>  esc_html__('Twitter ID:', 'city-girl'),
        'section'   =>  'city_girl_post_snippet_customization_section',
        'Settings'  =>  'twitter_id',
        'type' => 'text',
        'active_callback' => function () {
            return get_theme_mod('post_snippet_hide_show_social_share', city_girl_get_default_post_snippet_social_share());
        }
    ));
}
