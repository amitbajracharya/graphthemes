<?php

add_action('customize_register', 'city_girl_post_snippet_author');
function city_girl_post_snippet_author($wp_customize)
{

    $wp_customize->add_setting('post_snippet_hide_show_author', array(
        'sanitize_callback'     =>  'city_girl_sanitize_checkbox',
        'default'               =>  city_girl_get_default_post_snippet_author()
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'post_snippet_hide_show_author', array(
        'label' => esc_html__('Show/Hide Author', 'city-girl'),
        'section' => 'city_girl_post_snippet_customization_section',
        'settings' => 'post_snippet_hide_show_author',
        'type' => 'toggle',
    )));
}
