<?php

/* Default Post Snippet Author*/
function city_girl_get_default_post_snippet_author()
{
    return true;
}

/* Default Post Snippet Category*/
function city_girl_get_default_post_snippet_category()
{
    return true;
}

/* Default Post Snippet Comment*/
function city_girl_get_default_post_snippet_comment()
{
    return true;
}

/* Default Post Snippet Date*/
function city_girl_get_default_post_snippet_date()
{
    return true;
}

/* Default Post Snippet Featured Image*/
function city_girl_get_default_post_snippet_featured_image()
{
    return true;
}

/* Default Post Snippet Tag*/
function city_girl_get_default_post_snippet_featured_image_size()
{
    return esc_html__('large', 'city-girl');
}

/* Default Post Snippet Tag*/
function city_girl_get_default_post_snippet_tag()
{
    return true;
}

/* Default Post Snippet Excerpt Szie  */
function city_girl_get_default_post_snippet_excerpt_size()
{
    return 25;
}

/* Default Post Snippet Read More */
function city_girl_get_default_post_snippet_show_hide_read_more()
{
    return true;
}

/* Default Post Snippet Read More Text */
function city_girl_get_default_post_snippet_read_more_text()
{
    return esc_html__("Read More", 'city-girl');
}

/* Default Post Snippet Social Share */
function city_girl_get_default_post_snippet_social_share()
{
    return true;
}

/* Default Post Snippet Social Share Options*/
function city_girl_get_default_post_snippet_social_share_options()
{
    return array();
}
