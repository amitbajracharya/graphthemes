<?php

/* Default General Customization */

function city_girl_get_default_breadcrumbs()
{
    return false;
}


function city_girl_get_default_sticky_menu()
{
    return true;
}


function city_girl_get_default_container_width()
{
    return 1400;
}
