<?php

add_action('customize_register', 'city_girl_sticky_menu');
function city_girl_sticky_menu($wp_customize)
{

    $wp_customize->add_setting('city_girl_sticky_menu_option', array(
        'sanitize_callback'     =>  'city_girl_sanitize_checkbox',
        'default'               =>  city_girl_get_default_sticky_menu(),
    ));

    $wp_customize->add_control(new Graphthemes_Toggle_Control($wp_customize, 'city_girl_sticky_menu_option', array(
        'label' => esc_html__('Enable Sticky Menu', 'city-girl'),
        'section' => 'city_girl_general_customization_section',
        'settings' => 'city_girl_sticky_menu_option',
        'type' => 'toggle',
    )));
}
