<?php

function city_girl_customizer_upgrade_to_pro($wp_customize)
{

	$wp_customize->add_section(new City_Girl_Button_Control($wp_customize, 'upgrade-to-pro',	array(
		'title'    => esc_html__('★ City Girl Pro ', 'city-girl'),
		'type'	=> 'button',
		'pro_text' => esc_html__('Upgrade to Pro', 'city-girl'),
		'pro_url'  => esc_url('https://graphthemes.com/city-girl/'),
		'priority' => 1
	)));
}
add_action('customize_register', 'city_girl_customizer_upgrade_to_pro');


function city_girl_enqueue_custom_admin_style()
{
	wp_register_style('city-girl-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.css', false);
	wp_enqueue_style('city-girl-button');

	wp_enqueue_script('city-girl-button', get_template_directory_uri() . '/inc/blocks/includes/button/button.js', array('jquery'), false, true);
}
add_action('admin_enqueue_scripts', 'city_girl_enqueue_custom_admin_style');
