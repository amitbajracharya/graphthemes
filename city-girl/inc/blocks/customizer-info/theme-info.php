<?php

add_action('customize_register', 'city_girl_customizer_theme_info');

function city_girl_customizer_theme_info($wp_customize)
{

	$wp_customize->add_section('city_girl_theme_info_section', array(
		'title'       => esc_html__('❂ Theme Info', 'city-girl'),
		'priority' => 2
	));


	$wp_customize->add_setting('theme_info', array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
	));

	$theme_info = '';

	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__('Theme Details', 'city-girl') . ': </label><a class="button alignright" href="' . esc_url('https://graphthemes.com/city-girl/') . '" target="_blank">' . esc_html__('Click Here', 'city-girl') . '</a></span><hr>';

	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__('How to use', 'city-girl') . ': </label><a class="button alignright" href="' . esc_url('https://graphthemes.com/theme-docs/city-girl/') . '" target="_blank">' . esc_html__('Click Here', 'city-girl') . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__('View Demo', 'city-girl') . ': </label><a class="button alignright" href="' . esc_url('https://graphthemes.com/preview/?product_id=city-girl') . '" target="_blank">' . esc_html__('Click Here', 'city-girl') . '</a></span><hr>';
	$theme_info .= '<span class="sticky_info_row wp-clearfix"><label class="row-element">' . esc_html__('Support Forum', 'city-girl') . ': </label><a class="button alignright" href="' . esc_url('https://wordpress.org/support/theme/city-girl') . '" target="_blank">' . esc_html__('Click Here', 'city-girl') . '</a></span><hr>';

	$wp_customize->add_control(new City_Girl_Custom_Text($wp_customize, 'theme_info', array(
		'section' => 'city_girl_theme_info_section',
		'label' => $theme_info
	)));
}
