<?php


if (!defined('CITY_GIRL_WIDGET_PATH')) {
    define('CITY_GIRL_WIDGET_PATH', dirname(__FILE__));
}


/**
 * Author Profile Widget.
 */
require_once CITY_GIRL_WIDGET_PATH . '/includes/class-graphthemes-widget-functions.php';

require_once CITY_GIRL_WIDGET_PATH . '/includes/widgets/widget-author-profile.php';

require_once CITY_GIRL_WIDGET_PATH . '/includes/widgets/widget-recent-posts.php';

require_once CITY_GIRL_WIDGET_PATH . '/includes/widgets/widget-popular-posts.php';
